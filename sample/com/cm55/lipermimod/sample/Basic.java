package com.cm55.lipermimod.sample;

import java.io.*;

import com.cm55.lipermimod.*;

public class Basic {

  /** 接続に使用するポート番号。何でも良いがオリジナルのlipermiでは4455になっている */
  public static final int PORT = 4455;
  
  /** サーバが公開するオブジェクトの名称 */
  public static final String GLOBAL = "serverGlobal";
  
  public static void main(String[]args) throws IOException {
    
    // サーバを作成し、グローバルオブジェクトを作成して公開し、4455でクライアントの接続を待つ
    Server server = new Server();
    server.registerGlobal(GLOBAL, new ServerGlobalImpl());
    server.bind(PORT);

    // クライアントを作成し、サーバに接続する。当然ながらサーバはlocalhost
    // サーバの公開するオブジェクトを取得し、そのメソッドを呼び出す。
    Client client = new Client();    
    client.connect("localhost", PORT);    
    ServerGlobal serverGlobal = client.getGlobal(GLOBAL);
    System.out.println("client side:" + serverGlobal.hello("hello"));
  }
  
  /** サーバの公開するオブジェクトのインターフェース。必ずIRemoteを実装する必要がある */
  public static interface ServerGlobal extends IRemote {
    public String hello(String value);
  }

  /** サーバによる公開オブジェクトの実装 */
  private static class ServerGlobalImpl implements ServerGlobal {
    public String hello(String value) {
      System.out.println("server side:" + value);
      return value + " world";
    }
  }
}
