package com.cm55.lipermimod;

import static org.junit.Assert.*;

import org.junit.*;

public class ClientServerTest {

  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test() throws Exception {    
    // サーバ側
    server.registerGlobal("serverGlobal",  new ServerGlobalImpl());
    server.bind(4455);

    // クライアント側
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");
    //ystem.out.println("" +serverGlobal.hello(new ClientExportImpl()));
    {
      Object object2 = serverGlobal.loopBack(serverGlobal);    
      //ystem.out.println("" + (serverGlobal == object2));
    }
    {    
      ClientExport clientExport = new ClientExportImpl();
      Object object = serverGlobal.loopBack(clientExport);
      assertEquals(clientExport, object);
    }
    
    {
      ClientExport clientExport = new ClientExportImpl();
      serverGlobal.store(clientExport);
      assertEquals(clientExport, serverGlobal.load());
    }
    
    server.close();
  }
  
  public static interface ServerGlobal extends IRemote {
    public String hello(ClientExport cl);
    public Object loopBack(Object o);
    public void store(Object o);
    public Object load();
  }
  
  private static class ServerGlobalImpl implements ServerGlobal {
    private Object stored;
    public String hello(ClientExport cl) {
      //ystem.out.println("hello");
      cl.sample();
      return "world";
    }
    public Object loopBack(Object o) {
      return o;
    }
    @Override
    public void store(Object o) {
      stored = o;
    }
    @Override
    public Object load() {
      return stored;
    }
  }
  
  public static interface ClientExport extends IRemote {
    public void sample();
  }
  
  public static class ClientExportImpl implements ClientExport {
    public void sample() {
      //ystem.out.println("sample");
    }
  }

}
