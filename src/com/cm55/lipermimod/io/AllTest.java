package com.cm55.lipermimod.io;

import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Suite.*;


@RunWith(Suite.class)
@SuiteClasses( { 
  PullStream1Test.class,
  PullStream2Test.class,
  PullStream3Test.class,
  PushStream1Test.class,
  PushStream2Test.class,
})
public class AllTest {
  public static void main(String[] args) {
    JUnitCore.main(AllTest.class.getName());
  }
}