package com.cm55.lipermimod.io;

import java.io.*;

import com.cm55.lipermimod.*;

/**
 * {@link InputStream}をラップして、{@link PullStream}とするためのオブジェクト。
 * データ転送終了時に{@link InputStream}を閉じるかどうかを指定する。
 * @author y.sugimura
 */
public class PullStreamSource implements PullStream {

  /** 入力ストリーム */
  private InputStreamByteReader inputStreamToBytes;

  /** 
   * InputStream、クローズ操作を指定する。
   * @param in 入力ストリーム
   * @param close 処理終了時に入力ストリームをクローズする。
   */
  public PullStreamSource(InputStream in) {
    inputStreamToBytes = new InputStreamByteReader(in);
  }

  /**
   * 相手側から呼び出されるメソッド
   * @param size 取得するバイトサイズ
   * @return 取得したデータバイト列。データが無いときはnull
   */
  public synchronized byte[]input(int size) {
    return inputStreamToBytes.getBytes(size);
  }

  @Override
  public synchronized void unreferenced() {
    close();
  }
  
  public synchronized void close() {
    if (inputStreamToBytes == null) return;
    try {
      inputStreamToBytes.close();
    } catch (IOException ex) {
      throw new LipeRMIException.IOException(ex);
    }
    inputStreamToBytes = null;
  }

  /** デフォルトで全体サイズ０を返す　*/
  @Override
  public long getTotalSize() {
    return 0;
  }
}
