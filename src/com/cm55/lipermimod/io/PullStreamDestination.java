package com.cm55.lipermimod.io;

import java.io.*;

/**
 * {@link PullStream}の説明を参照のこと。
 * <p>
 * 以下では、クライアントからサーバにデータを送ることを前提に説明する。つまり、最初にクライアントは{@link PullStream}オブジェクトをサーバ側に送り込み、
 * サーバ側ではこれを読み出すことにより、その結果クライアント側のデータを読み出すことになる。
 * </p>
 * <p>
 * サーバ側では受け取った{@link PullStream}をこのオブジェクトにラップすることで、いかにも通常の{@link InpuStream}であるかのように扱うことができる。
 * 単なる{@link InputStream}であるため、通常の読み出しを行い、終了時にはclose()すること。
 * </p>
 * @author y.sugimura
 */
public class PullStreamDestination extends InputStream {

  /** ラップされた{@link PullStream} */
  private PullStream stream;
  
  /** 入力時に指定する読み取り長さ。特に変更する必要は無い */
  private int inputRequestLength = 1024 * 16;
  
  /** 読み取りバッファ。このバッファは固定的なものではなく、リモートから入力するたびに新たに作成されることに注意 */
  private byte[]readBuffer;
  
  /** バッファ中の有効データの開始位置。readBuffer.length - readOffsetが有効データサイズになる */
  private int readOffset;

  /**
   * {@link PullStream}をラップする。
   * @param stream
   */
  public PullStreamDestination(PullStream stream) {
    this.stream = stream;
  }

  @Override
  public int read() throws IOException {
    if (!ensureBuffer()) return -1;
    return ((int)readBuffer[readOffset++]) & 0xff;
  }

  /** 
   * {@link InputStream}の規約通り、writeBufferのwriteOffset位置からrequestSizeのサイズにできるだけデータを詰め込み、
   * 詰め込んだサイズを返す。ストリーム終端であれば、-1を返す。
   */
  @Override
  public int read(byte[]writeBuffer, int writeOffset, int requestSize) throws IOException {
    if (!ensureBuffer()) return -1;    
    int writtenSize = 0;
    while (requestSize > 0) {
      if (!ensureBuffer()) return writtenSize;      
      int thisTime = Math.min(requestSize, readBuffer.length - readOffset);
      System.arraycopy(readBuffer, readOffset, writeBuffer, writeOffset, thisTime);
      readOffset += thisTime;
      writeOffset += thisTime;
      requestSize -= thisTime;
      writtenSize += thisTime;
    }
    return writtenSize;
  }

  @Override
  public void close() {
    stream.close();
  }
  
  /** 
   * 読み取りバッファに有効データのあることを確実にする。もはやデータが無い場合はfalseを返す。
   * @return true:有効データがあり、その位置はreadOffset、サイズはreadBuffer.length - readOffset、false:データがもうない
   * @throws IOException
   */
  private boolean ensureBuffer() throws IOException {
    // まだ有効データがある場合は、何もせずtrueを返す。
    if (readBuffer != null && readOffset < readBuffer.length)
      return true;    
    
    // 要望する読み取り長を指定して読み取る。必ずしも、その長さが読み取られるわけではない。
    // データがもう無い場合にはnullが返される
    readBuffer = stream.input(inputRequestLength);
    if (readBuffer == null) return false;
    
    // 読み取り開始位置をリセットする
    readOffset = 0;      
    return true;
  }
}
