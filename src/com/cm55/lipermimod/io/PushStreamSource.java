package com.cm55.lipermimod.io;

import java.io.*;

/**
 * {@link PushStream}を{@link OutputStream}として使用し、
 * この{@link OutputStream}に書き込まれたデータを押し込むためのオブジェクト
 * @author y.sugimura
 */
public class PushStreamSource extends OutputStream {

  /** ラップする{@link PushStream} */
  private final PushStream pushStream;
  
  /** バッファサイズ */
  private int bufferSize = 1024 * 16;
  
  /** バッファ */
  private byte[]writeBuffer = null;
  
  /** バッファ充填済みサイズ */
  private int writenLength = 0;

  /** {@link PushStream}をラップする */
  public PushStreamSource(PushStream pushStream) {
    this(pushStream, 0);
  }

  /** {@link PushStream}をラップすると同時にサイズを指定する
   * @param pushStream
   * @param totalSize
   */
  public PushStreamSource(PushStream pushStream, long totalSize) {
    this.pushStream = pushStream;    
    pushStream.setTotalSize(totalSize);
  }

  public PushStreamSource setBufferSize(int size) {
    bufferSize = size;
    return this;
  }
  
  
  /** 1バイトを出力 */
  @Override
  public void write(int c) throws IOException {    
    byte[]b = new byte[] { (byte)c };
    write(b, 0, 1);
  }
  
  /** 複数バイト出力 */
  @Override
  public void write(byte[]bytes, int offset, int length) throws IOException {    
    int endpos = offset + length;
    while (offset < endpos) {
      if (writeBuffer == null) {
        writeBuffer = new byte[bufferSize];    
        writenLength = 0;
      }
      
      int fillSize = Math.min(writeBuffer.length - writenLength, endpos - offset);
      System.arraycopy(bytes, offset, writeBuffer, writenLength, fillSize);
      writenLength += fillSize;
      offset += fillSize;
      if (writenLength == writeBuffer.length) {
        internalFlush();
      }
    }
  }
  
  /** 上位からのフラッシュ指示 */
  @Override 
  public void flush() throws IOException {
    internalFlush();
  }
  
  /** クローズ */
  @Override
  public void close() throws IOException {
    internalFlush();
    pushStream.close();
  }
  
  /** 
   * バッファの内容を書き出す。  
   */
  private void internalFlush() throws IOException {
    if (writeBuffer == null) 
      return;
   
    if (writenLength == writeBuffer.length) {
      pushStream.output(writeBuffer);
    } else {
      byte[]bytes = new byte[writenLength];
      System.arraycopy(writeBuffer, 0, bytes, 0, writenLength);
      pushStream.output(bytes);
    }
    
    writeBuffer = null;
    writenLength = 0;
  }
}
