package com.cm55.lipermimod.io;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.*;

import com.cm55.lipermimod.*;

public class PullStream2Test {


  Server server;
  Client client;
    
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test2() throws Exception { 
    // ServerGlobalImplを作成して、サーバスタート
    ServerGlobalImpl2 serverGlobalImpl = new ServerGlobalImpl2();
    server.registerGlobal("serverGlobal",  serverGlobalImpl);
    server.bind(4455);

    // クライアントがサーバに接続して、インターフェース取得
    client.connect("localhost",  4455);
    ServerGlobal2 serverGlobal = client.getGlobal("serverGlobal");

    // アップロードを行う。
    byte[]uploading = new byte[3333];    
    boolean[]closed = new boolean[1];
    IFuture<String> future = new IFuture<String>() {
      public void exception(Throwable th) {
        //ystem.out.println("returned");
      }

      @Override
      public void returns(String o) {
        //ystem.out.println("returns " + o);
      }
    };
    serverGlobal.upload(future, new PullStreamSource(new ByteArrayInputStream(uploading)) {
      public void close() {
        super.close();
        closed[0] = true;
      }
    });
    //ystem.out.println("upload return");
    
    // サーバ側の終了を待つ
    serverGlobalImpl.waitFor();
    
    // アップロードデータが一致するか
    assertArrayEquals(uploading, serverGlobalImpl.uploaded);

    // クローズされた
    assertTrue(closed[0]);
    
    // サーバクローズ
    server.close();
  }
  
  public interface ServerGlobal2 extends IRemote {    
    public String upload(IFuture<String> future, PullStream stream);    
  }
  
  public static class ServerGlobalImpl2 implements ServerGlobal2 {
    byte[]uploaded;
    public String upload(IFuture<String> future, PullStream stream) { 
      try (ByteArrayOutputStream bytes = new ByteArrayOutputStream();
          PullStreamDestination client = new PullStreamDestination(stream)) {
        //ystem.out.println("started");
        copy(client, bytes);
        uploaded = bytes.toByteArray();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
      synchronized(this) {
        this.notify();
      }
      return "Finished";
    } 
    public synchronized void waitFor() {
      try { this.wait(); } catch (Exception ex) {}
    }
  }
  
  public static void copy(InputStream in, OutputStream out) throws IOException {
    byte[]buf = new byte[4096];
    while (true) {
      int size = in.read(buf);
      if (size <= 0) break;
      out.write(buf, 0, size);
    }
  }

}
