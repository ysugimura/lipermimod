package com.cm55.lipermimod.io;

import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.junit.*;

import com.cm55.lipermimod.*;

public class PullStream3Test {
  
  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test() throws Exception { 
    // ServerGlobalImplを作成して、サーバスタート
    ServerGlobalImpl serverGlobalImpl = new ServerGlobalImpl();
    server.registerGlobal("serverGlobal",  serverGlobalImpl);
    server.bind(4455);

    // クライアントがサーバに接続して、インターフェース取得
    client.connect("localhost",  4455);
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");

    // データをダウンロードする
    byte[]downloaded;
    try (ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        InputStream in = new PullStreamDestination(serverGlobal.download())) {
      copy(in, bytes);
      downloaded = bytes.toByteArray();
    } catch (IOException ex) {
      ex.printStackTrace();
      return;
    }

    // アップロードデータが一致するか
    assertArrayEquals(downloaded, serverGlobalImpl.downloading);
    
    // サーバクローズ
    server.close();
  }

  public interface ServerGlobal extends IRemote {    
    public PullStream download();
  }
  
  public static class ServerGlobalImpl implements ServerGlobal {
    byte[]downloading = randomBytes(3333);
    public PullStream download() {
      return new PullStreamSource(new ByteArrayInputStream(downloading));
    } 
  }
  
  static byte[] randomBytes(int size) {
    byte[]buffer = new byte[size];
    Random r = new Random();
    IntStream.range(0,  size).forEach(i->buffer[i] = (byte)r.nextInt());
    return buffer;
  }
  
  static void copy(InputStream in, OutputStream out) throws IOException {
    byte[]buf = new byte[4096];
    while (true) {
      int size = in.read(buf);
      if (size <= 0) break;
      out.write(buf, 0, size);
    }
  }
}
