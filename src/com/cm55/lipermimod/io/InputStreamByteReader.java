package com.cm55.lipermimod.io;

import java.io.*;

import com.cm55.lipermimod.*;

/**
 * <p>
 * 入力ストリームから読み込み、バイト配列に変換する。
 * また、現在の読み込み済みバイト数を保持する。
 * </p>
 * <p>
 * {@link InputStream}からの読み込みでは、単一のバッファを使い回すことが一般的である。つまり、
 * </p>
 * <pre>
 * byte[]buffer = new byte[...];
 * while (true) {
 *   int r = in.read(buffer);
 *   ...
 * }
 * </pre>
 * <p>
 * つまり、読み取り希望サイズのバッファに対し、実際に読み取られるデータサイズは異なる可能性がある。
 * </p>
 * <p>
 * しかし、このシステムでリモートにデータを送るためには、バイト配列を読み込むたびに、そのサイズのバイト配列オブジェクトを作成したい。
 * そのため、入力ストリームに対して、読み取りサイズを指定してデータが読み取られたら、その読み取られたデータのサイズと同じサイズのバイト配列を
 * 作成して返す。
 * </p>
 * <p>
 * またリモートへのデータ転送という長時間の操作の進捗をはかるため、読み取られたバイト数を常に保持する。
 * </p>
 * @author ysugimura
 */
class InputStreamByteReader {
  
  /** 入力ストリーム*/
  private final InputStream in;
  
  /** 読み込み済みバイトサイズ*/
  private long progress;
  
  /** 入力ストリームを指定する */
  InputStreamByteReader(InputStream in) {
    this.in = in;
  }
  
  /** このオブジェクトが保持しているInputStreamを取得する */
  public InputStream getInputStream() {
    return in;
  }
  
  public void close() throws IOException {
    in.close();
  }
  
  /** 
   * 指定されたサイズのバイト列を読み込む。
   * データが無い場合はnullを返す。
   * @param requireSize 読み込み要求サイズ
   * @return データバイト列。データが無ければnull
   * @throws IOException
   */
  byte[]getBytes(int requireSize) {
    // 希望サイズのバッファを作成する
    byte[]readBytes = new byte[requireSize];
    int readSize;
    try {
      readSize = in.read(readBytes);
    } catch (IOException ex) {
      throw new LipeRMIException.IOException(ex);
    }
    
    // データがもう無い場合nullを返す。
    if (readSize <= 0) return null;
    
    // 読み取り済バイト数を進める
    progress = readSize;
    
    // 希望サイズ通りのサイズであった場合、バッファ自体を返す。
    if (readSize == requireSize) return readBytes;
    
    // そうでない場合、新たなバッファを作成して返す。
    byte[]result = new byte[readSize];
    System.arraycopy(readBytes, 0, result, 0, readSize);
    return result;
  }

  /** 現在の読み取られたバイト数を返す */
  long getProgress() {
    return progress;
  }
}
