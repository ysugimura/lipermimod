package com.cm55.lipermimod.io;

import java.io.*;

/**
 * {@link OutputStream}をラップし、リモートから書き込まれるデータをラップした{@link OutputStream}に
 * 書き込むためのオブジェクト。書き込み側が最初にサイズを提供してくる場合には、進捗パーセンテージコールバックが行われる。
 * 
 * @author ysugimura
 */
public class PushStreamDestination implements PushStream {

  protected long totalSize;
  private OutputStream output;
  private boolean closed;
  private int currentPercent;
  private long bytesWritten;

  /** {@link OutputStream}をラップする */
  public PushStreamDestination(OutputStream output) {
    this.output = output;
  }

  /** サイズの指定。パーセントコールバックに用いられる。ただし、データ書き込み以前でなくてはいけない */
  @Override
  public void setTotalSize(long totalSize) {
    if (bytesWritten > 0) return;
    this.totalSize = totalSize;
    percent(0);
  }

  /** データ出力 */
  @Override
  public void output(byte[]bytes) throws IOException {
    output.write(bytes);
    bytesWritten += bytes.length;
    if (totalSize == 0) return;
    int newPercent = (int)(bytesWritten * 100 / totalSize);
    if (currentPercent != newPercent) {
      percent(currentPercent = newPercent);
    }
  }

  /** 進捗コールバック */
  protected void percent(int value) {    
  }

  /** クローズ指示 */
  @Override
  public void close() throws IOException {
    if (!closed)
      output.close();
    closed = true;
  }
}
