package com.cm55.lipermimod.io;


import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.junit.*;

import com.cm55.lipermimod.*;

public class PushStream1Test {
  
  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test() throws Exception { 
    ServerGlobalImpl serverGlobalImpl = new ServerGlobalImpl();
    server.registerGlobal("serverGlobal",  serverGlobalImpl);
    server.bind(4455);

    client.connect("localhost",  4455);
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");

    // ダウンロードを行う。
    ByteArrayOutputStream downloaded = new ByteArrayOutputStream();
    serverGlobal.download(new PushStreamDestination(downloaded));
    // ダウンロードが終了するまでここには帰ってこない。

    // ダウンロードデータが一致するか
    assertArrayEquals(downloaded.toByteArray(), serverGlobalImpl.downloading);
    
    // サーバクローズ
    server.close();
  }

  public interface ServerGlobal extends IRemote {    
    public void download(PushStream stream) throws IOException;    
  }
  
  public static class ServerGlobalImpl implements ServerGlobal {
    byte[]downloading = randomBytes(13333);
    public void download(PushStream stream) throws IOException { 
      try (PushStreamSource output = new PushStreamSource(stream)) {
        output.write(downloading);
      }
    } 
  }

  static byte[] randomBytes(int size) {
    byte[]buffer = new byte[size];
    Random r = new Random();
    IntStream.range(0,  size).forEach(i->buffer[i] = (byte)r.nextInt());
    return buffer;
  }
}