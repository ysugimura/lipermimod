package com.cm55.lipermimod.io;


import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.junit.*;

import com.cm55.lipermimod.*;

public class PushStream2Test {
  
  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test() throws Exception { 
    ServerGlobalImpl serverGlobalImpl = new ServerGlobalImpl();
    server.registerGlobal("serverGlobal",  serverGlobalImpl);
    server.bind(4455);

    client.connect("localhost",  4455);
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");

    // アップロードを行う。
    byte[]uploading = randomBytes(13333);
    try (OutputStream out = new PushStreamSource(serverGlobal.getForUpload())) {
      out.write(uploading);
    }

    // アップロードデータが一致するか
    assertArrayEquals(uploading, serverGlobalImpl.uploaded.toByteArray());
    
    // サーバクローズ
    server.close();
  }

  public interface ServerGlobal extends IRemote {    
    public PushStream getForUpload() throws IOException;    
  }
  
  public static class ServerGlobalImpl implements ServerGlobal {
    ByteArrayOutputStream uploaded = new ByteArrayOutputStream();
    public PushStream getForUpload() throws IOException {       
      return new PushStreamDestination(uploaded);
    } 
  }

  static byte[] randomBytes(int size) {
    byte[]buffer = new byte[size];
    Random r = new Random();
    IntStream.range(0,  size).forEach(i->buffer[i] = (byte)r.nextInt());
    return buffer;
  }
}