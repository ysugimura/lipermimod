package com.cm55.lipermimod.io;

import java.io.*;

import com.cm55.lipermimod.*;

/**
 * リモートに対してデータを押し込むためのインターフェース
 * <p>
 * サーバからクライアントにデータストリームをダウンロードしたい場合には、クライアントから{@link PushStream}オブジェクトをサーバ側に送る。
 * サーバ側では、このオブジェクトにデータを押し込み、クライアント側ではこれを受け取る。
 * </p>
 * <p>
 * 逆に、クライアントからデータをサーバに送りたい場合には、あるサーバのメソッドの返り値として{@link PushStream}をクライアント側に送信し、
 * クライアント側では、このオブジェクトにデータを押し込む。しかし、この目的としては{@link PullStream}の方が適切と思われる。
 * </p>
 * <p>
 * 以下では簡単のために、前者の用途、サーバからクライアントにデータをダウンロードすることを前提に説明する。
 * </p>
 * <h2>クライアント側で用意する{@link PushStream}オブジェクト</h2>
 * <p>
 * クライアント側はデータの到着側であり、この目的のため、{@link PushSteam}を実装するクラスとして、{@link PushStreamDestination}
 * がある。これは単純に{@link OutputStream}をラップするものであり、サーバから送られたデータがクライアント側の{@link OutputStream}
 * に書き込まれる。{@link PushStreamDestination}を参照のこと。
 * </p>
 * <h2>サーバ側で行う操作 </h2>
 * <p>
 * サーバ側はデータの発信側であり、この目的のため、{@link PushStream}を{@link PushStreamSource}にラップする。これは通常の{@link OutputStream}
 * であるので、通常の操作で出力を行えばよい。
 * </p>
 * 
 * @author y.sugimura
 */
public interface PushStream extends IRemote {
  
  /** 全体サイズを通知する。あるいは通知しない */
  public void setTotalSize(long size);
  
  /** バイト列を押し込む */
  public void output(byte[]data) throws IOException;
  
  /** ストリームの終了を示す */
  public void close() throws IOException;
}
