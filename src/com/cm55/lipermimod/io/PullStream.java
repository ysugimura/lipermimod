package com.cm55.lipermimod.io;

import java.io.*;

import com.cm55.lipermimod.*;

/**
 * リモートに対してデータを取得してもらうためのインターフェース
 * <p>
 * 例えば、サーバからクライアントにデータをアップロードしたい場合には、クライアントから{@link PullStream}オブジェクトをサーバ側に送る。
 * サーバ側では、このオブジェクトからデータを読み込む。クライアントでも読み込み操作が行われる。
 * </p>
 * <p>
 * 逆に、クライアントがサーバからデータダウンロードしたい場合には、あるサーバのメソッドの返り値として{@link PullStream}をクライアント側に送信し、
 * クライアント側では、このオブジェクトからデータを読み込む。サーバ側でも読み込み操作が起こる。しかし、この目的としては{@link PushStream}の方が適切と思われる。
 * </p>
 * <p>
 * 通常は、前者の使い方が一般的と思われる。
 * </p>
 * @author y.sugimura
 */
public interface PullStream extends IUnreferenced {

  /**
   * 全体サイズが要求される。不明の場合は0を返す。
   * @return 全体サイズ、あるいは0
   */
  public long getTotalSize();
  
  /** 
   * 指定サイズのデータバイト配列が要求されるので、それを返す。指定サイズより小さい場合がある。無い場合はnullを返す。
   * @param requireSize 要求サイズ
   * @return 要求サイズ以下のデータバイト配列。あるいはnull
   */
  public byte[]input(int requireSize) throws IOException;

  /** クローズを指示される。 */
  public void close();
}
