package com.cm55.lipermimod.io;

import static org.junit.Assert.*;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import org.junit.*;

import com.cm55.lipermimod.*;

public class PullStream1Test {
  
  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test() throws Exception { 
    // ServerGlobalImplを作成して、サーバスタート
    ServerGlobalImpl serverGlobalImpl = new ServerGlobalImpl();
    server.registerGlobal("serverGlobal",  serverGlobalImpl);
    server.bind(4455);

    // クライアントがサーバに接続して、インターフェース取得
    client.connect("localhost",  4455);
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");

    // 適当なデータをアップロードする
    byte[]uploading = randomBytes(3333);   
    serverGlobal.upload(new PullStreamSource(new ByteArrayInputStream(uploading)));
    // アップロードが終了するまでここには帰ってこない。

    // アップロードデータが一致するか
    assertArrayEquals(uploading, serverGlobalImpl.uploaded);
    
    // サーバクローズ
    server.close();
  }

  public interface ServerGlobal extends IRemote {    
    public void upload(PullStream stream);    
  }
  
  public static class ServerGlobalImpl implements ServerGlobal {
    byte[]uploaded;
    public void upload(PullStream stream) { 
      try (ByteArrayOutputStream bytes = new ByteArrayOutputStream();
          InputStream client = new PullStreamDestination(stream)) {
        copy(client, bytes);
        uploaded = bytes.toByteArray();
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    } 
  }
  
  static byte[] randomBytes(int size) {
    byte[]buffer = new byte[size];
    Random r = new Random();
    IntStream.range(0,  size).forEach(i->buffer[i] = (byte)r.nextInt());
    return buffer;
  }
  
  static void copy(InputStream in, OutputStream out) throws IOException {
    byte[]buf = new byte[4096];
    while (true) {
      int size = in.read(buf);
      if (size <= 0) break;
      out.write(buf, 0, size);
    }
  }
}
