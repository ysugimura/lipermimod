package com.cm55.lipermimod;

import java.io.*;
import java.net.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.exported.*;
import com.cm55.lipermimod.handler.*;
import com.cm55.lipermimod.option.*;
import com.cm55.lipermimod.server.*;

public class Server {

  private static final Log log = LogFactory.getLog(Server.class);
  
  /** サーバソケット */
  private ServerSocket serverSocket;
  
  /** バインドスレッド */
  private Thread bindThread;

  /** 接続中クライアントのストック */
  private ClientStock clientStock = new ClientStock();
  
  private ExportedObjects exportedObjects;
  private GlobalExports globalExports;  
  private IProtocolFilter filter = new DefaultFilter();
  
  private String name = "unknown " + System.identityHashCode(this);
  
  /** サーバを作成 */
  public Server() {
    this.exportedObjects = new ExportedObjects();
    globalExports = new GlobalExports(exportedObjects);
    exportedObjects.registerSystemServiceExported(new SystemServiceImpl(globalExports)).setGlobal(true);
  }

  /** フィルタを指定する */
  public Server setFilter(IProtocolFilter filter) {
    beforeBind();
    this.filter = filter;
    return this;
  }

  public Server setName(String name) {
    beforeBind();
    this.name = name;
    return this;
  }
  
  /** 
   * ハートビート時間切れ秒数を設定
   * 0(デフォルト)はハートビートをチェックしない。
   * ０以外に設定すると、クライアントからのハートビートがこの時間こなければ、
   * そのクライアントが死んだものとみなす。
   * 当然のことながら、クライアント側のハートビート設定があわせて必要。
   */
  public Server setHeartBeatTimeout(int seconds) {
    beforeBind();
    clientStock.setHeartBeatTimeout(seconds);
    return this;
  }
  
  private void beforeBind() {
    if (serverSocket != null) throw new IllegalStateException();
  }

  /** 
   * オブジェクトをglobalNameという名称でグローバル登録する。
   * 既に同じ名前でグローバル登録されているオブジェクトがあれば、その登録は解除される。 
   * オブジェクトは{@link IRemote}を拡張するインターフェースを一つだけ実装していなければならない。
   * @param globalName グローバル名
   * @param object  登録するオブジェクト
   * @return
   */
  public <T extends IRemote>Server registerGlobal(String globalName, T object) {
    if (globalName == null || object == null)
      throw new NullPointerException();
    globalExports.registerGlobal(globalName, object);
    return this;
  }

  /** 指定名称のグローバル登録を解除する。登録されていなければ何もしない 
   * @param globalName 登録済のグローバル名
   * @return 
   */
  public Server unregisterGlobal(String globalName) {
    if (globalName == null) throw new NullPointerException();
    globalExports.unregisterGlobal(globalName);
    return this;
  }


  /** 
   * 指定ポートにバインドする。
   * これ以降サーバはクライアントからの接続を受け付けて、そのリクエストを
   * 処理することになるが、この処理は独立したスレッドで行われるので、
   * bindの呼び出し自体はすぐ返ってくる。
   * @param port bindするポート番号
   * @return 本オブジェクト
   * @throws IOException
   */
  public Server bind(int port) throws IOException {
    
    // クライアントの接続監視をスタートする
    clientStock.startDisconnectDetection();

    // サーバソケットを作成する
    serverSocket = new ServerSocket();
    serverSocket.setPerformancePreferences(1, 0, 2);
    serverSocket.bind(new InetSocketAddress(port));

    // クライアントからの接続要求を調べるスレッドを起動する
    (bindThread = new Thread(new Runnable() {
      public void run() {
        while (true) {
          Socket clientSocket;
          try {
            clientSocket = serverSocket.accept();
          } catch (SocketException ex) {
            if (ex.getMessage().equals("socket closed")) {
              // ソケットクローズの場合はループを抜ける
              break;
            }
            // ソケットクローズ以外の例外
            log.error("Server Socket Exception", ex);
            continue;
          } catch (IOException e) {
            // IO例外
            log.error("Server Socket IO error", e);
            continue;
          }
          acceptClientConnection(clientSocket);
        }
      }            
    }, String.format("Bind (%d)", port))).start();
    
    return this;
  }
  

  /**
   * クライアントからの接続を受け入れる 
   * @param clientSocket
   */
  private void acceptClientConnection(final Socket clientSocket) {
    final ConnectionImpl handler = new ConnectionImpl();
        handler.setup(clientSocket, exportedObjects, filter, name);
    if (log.isTraceEnabled()) log.trace("Server gets connectionHandler " + handler);    
    handler.addConnectionHandlerListener(new IConnectionHandlerListener() {
      public void connectionClosed(Throwable th) {
        clientStock.removeClientHandler(handler);
      }
    });    
    clientStock.addClientHandler(handler);
  }

  /** サーバリスナを登録する */
  public Server addServerListener(IServerListener listener) {
    clientStock.addListener(listener);
    return this;
  }
  /** サーバリスナを削除する */
  public Server removeServerListener(IServerListener listener) {
    clientStock.removeListener(listener);
    return this;
  }

  /** サーバソケットをクローズし、処理スレッドの終了を待つ */
  public Server close() {
    clientStock.interruptDisconnectDetection();    
    if (serverSocket != null) {
      try {
        serverSocket.close();
      }   catch (IOException ex) {}
    }
    if (bindThread != null) {
        try {
          bindThread.join();
        } catch (InterruptedException ex) {}
    }
    return this;
  }
}

