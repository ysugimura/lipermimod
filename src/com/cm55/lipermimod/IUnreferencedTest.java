package com.cm55.lipermimod;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

public class IUnreferencedTest {

  Server server;
  Client client;
  List<String>seq;
  
  @Before
  public void before() {
    server = new Server().setName("サーバ");
    client = new Client().setName("クライアント");
    seq = Collections.synchronizedList(new ArrayList<String>());
  }
  
  @Test
  public void クライアントの参照を除去する() throws Exception {    
    
    // サーバ側
    server.registerGlobal("serverGlobal",  new ServerGlobalImpl(seq));
    server.bind(4455);

    // クライアント側
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");
    ServerSub serverSub = serverGlobal.getServerSub();
    
    serverSub.hello();
    
    seq.add("クライアントからserverSubへの参照を除去");
    serverSub = null;
    
    gcAndWait();
    
    server.close();

    assertArrayEquals(
      new String[] {
        // VMによってはこ順序で発生しないことがある。
        "helloが呼び出された",
        "クライアントからserverSubへの参照を除去",
        "GCの開始",
        "subのunrefrenced呼び出し",
        "subのfinalize呼び出し",
      },
      seq.toArray(new String[0])
    );
  }
  
  @Test
  public void クライアントを強制的に切断する() throws Exception {    
    
    // サーバ側
    server.registerGlobal("serverGlobal",  new ServerGlobalImpl(seq));
    server.bind(4455);

    // クライアント側
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");
    ServerSub serverSub = serverGlobal.getServerSub();    
    serverSub.hello();
    
    // クライアントをクローズするserverSubへの参照は保持したまま
    seq.add("クライアントをクローズ");
    client.close();
        
    gcAndWait();

    server.close();
    
    assertArrayEquals(
      new String[] {
        // VMによってはこ順序で発生しないことがある。
        "helloが呼び出された",
        "クライアントをクローズ",
        "GCの開始",
        "subのunrefrenced呼び出し",
        "subのfinalize呼び出し",
      },
      seq.toArray(new String[0])
    );
  }
  
  void gcAndWait() throws InterruptedException {
    seq.add("GCの開始");
    System.gc();
    Thread.sleep(100);
    System.gc();
  }
  
  public static interface ServerGlobal extends IRemote {
    public ServerSub getServerSub();
  }
  
  private static class ServerGlobalImpl implements ServerGlobal {
    List<String>seq;
    public ServerGlobalImpl(List<String>seq) {
      this.seq = seq;
    }
    public ServerSub getServerSub() {
      return new ServerSubImpl(seq);      
    }
  }

  public static interface ServerSub extends IUnreferenced {    
    public void hello();
  }
  
  public static class ServerSubImpl implements ServerSub {
    List<String>seq;
    public ServerSubImpl(List<String>seq) {
      this.seq = seq;
    }
    public void hello() {
      seq.add("helloが呼び出された");
    }
    public void unreferenced() {
      seq.add("subのunrefrenced呼び出し");
    }
    @Override
    protected void finalize() throws Throwable {
      super.finalize();
      seq.add("subのfinalize呼び出し");
    }
  }
}
