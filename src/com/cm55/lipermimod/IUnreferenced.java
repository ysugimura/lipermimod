package com.cm55.lipermimod;

/**
 * 相手側で不要になった場合に呼び出されるメソッド。
 * <p>
 * 例えば、リモート側のオブジェクトをローカル側で使用する場合、ローカル側にそのプロキシが作成されるが、このプロキシはWeakReferenceで保持されている。
 * そして、ローカル側からの参照が無くなった場合には、リモート側にそのことがunreferencedメソッド呼び出しとして伝えられる。
 * これは、そのオブジェクトが{@link IUnreferenced}を実装していようがしていまいが必ず起こる。
 * リモート側ではunreferencedメソッド呼び出しにより、ローカル側でもはや不要になったことがわかり、しかるべき措置が行わる。
 * このとき、もしそのオブジェクトが{@link IUnreferenced}を実装していた場合には、そのオブジェクトのunreferenced()メソッドも呼び出され、
 * オブジェクト側での必要は処理を行うことができる。
 * </p>
 * @author y.sugimura
 */
public interface IUnreferenced extends IRemote {
  public static final String UNREFERENCED = "unreferenced";
  public void unreferenced();
}
