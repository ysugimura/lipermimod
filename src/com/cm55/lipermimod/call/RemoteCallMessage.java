/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod.call;

/**
 * Class that holds method call informations.
 * 
 * @date   05/10/2006 
 * @author lipe
 * 
 * <p>
 * リモートメソッド呼び出しを行ったとき、その呼出しは{@link RemoteCallMessage}の形で通信路を伝搬される。
 * ここには、{@link RemoteHandle}として呼び出されるオブジェクトを示す記述、その呼び出しメソッド名、引数、呼び出しIDが格納される。
 * </p>
 * <p>
 * 呼び出しメソッド名は単純名である。リモートインターフェースにメソッドオーバーロードは認めていないため、引数型の情報は無い。
 * </p>
 */
public class RemoteCallMessage implements IRemoteMessage {
  private static final long serialVersionUID = -4057457700512552099L;

  /** 呼び出し先オブジェクトの記述子 */
  public final RemoteHandle remoteHandle;

  /** 呼び出すメソッド名。メソッドオーバーロードは認めないため、単純名 */
  public final String methodName;

  /** メソッドの引数。nullは存在しない */
  public final Object[] arguments;

  /** 呼び出しシーケンス番号 */
  public final long callId;

  /**
   * メソッド呼び出しメッセージを作成する
   * @param remoteHandle 呼び出す
   * @param methodName
   * @param args
   * @param callId
   */
  public RemoteCallMessage(RemoteHandle remoteHandle, String methodName, Object[] args, long callId) {
    if (remoteHandle == null || methodName == null || args == null) throw new NullPointerException();
    this.remoteHandle = remoteHandle;
    this.methodName = methodName;
    this.arguments = args;
    this.callId = callId;
  }

  @Override
  public String toString() {
    return RemoteCallMessage.class.getSimpleName() + ":" + callId + "," + remoteHandle + ", " + methodName;
  }
}
