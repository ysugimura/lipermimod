package com.cm55.lipermimod.call;

import com.cm55.lipermimod.*;


/**
 * システムサービスインターフェース
 * <p>
 * このインターフェースは特殊である。このシステムでは、相手側のサービスを呼び出すには、まずはじめに{@link ExportObjectDesc}を取得して、
 * 呼び出しメソッドを持つオブジェクトを得なければならない。しかし、最初の時点では、クライアントはサーバ側の何らのオブジェクトも所有していない。
 * だから、{@link Client#getGlobal(String)}でサーバ側の何らかのオブジェクトを取得しようとも、そもそもその機能を果たすオブジェクトを
 * クライアント側が持っていない。
 * つまり、サーバ側の何らかのオブジェクトの何らかのメソッドを呼び出そうにも、そのオブジェクトがクライアント側に存在しない。
 * </p>
 * <p>
 * これを解決するために、あらかじめサーバクライアント間で固定のオブジェクトを定義してある。これが{@link SystemService}オブジェクトであり、
 * この{@link ExportObjectDesc}として、あらかじめ{@link ExportObjectDesc#SYSTEM_SERVICE}が定義済になっている。
 * このオブジェクトに対して、{@link SystemService}のメソッドを呼び出すことができる。
 * </p>
 * @author y.sugimura
 */
public interface SystemService extends IRemote {
  
  /** 
   * 名前で指定された、あらかじめサーバ側に登録済のグローバルオブジェクトを取得する。
   * @param globalName サーバ側に登録済のオブジェクト名称
   * @return サーバ側登録済オブジェクトのインターフェース
   */
  public <T>T getGlobal(String globalName);
  
  /** 
   * ハートビートを送る 。
   * クライアント側から自動的・定期的に呼び出すだけであり、これによってサーバ側はクライアントが死んでいないことを確認する。
   * ただし、あらかじめサーバ側にはハートビートタイムアウトが設定されており、かつクライアント側には、ハートビート間隔が設定されていることが必要。
   */
  public void heartBeat();
}
