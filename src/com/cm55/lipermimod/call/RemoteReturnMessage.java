/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod.call;

/**
 * Class that holds method return information.
 * 
 * @date   05/10/2006 
 * @author lipe
 * @author y.sugimura
 */
public class RemoteReturnMessage implements IRemoteMessage {
  private static final long serialVersionUID = -2353656699817180281L;

  /** 返り値オブジェクトが例外を表しているか */
  public final boolean throwing;

  /** 返り値オブジェクトもしくは例外 */
  public final Object ret;

  /** 呼び出しシーケンス番号 */
  public final long callId;
  
  /** 返り値を作成 */
  private RemoteReturnMessage(boolean throwing, Object ret, long callId) {
    this.throwing = throwing;
    this.ret = ret;
    this.callId = callId;
  }
  
  public static RemoteReturnMessage returning(Object ret, long callId) {
    return new RemoteReturnMessage(false, ret, callId);
  }
  
  public static RemoteReturnMessage throwing(Object ret, long callId) {
    return new RemoteReturnMessage(true, ret, callId);
  }

  @Override
  public String toString() {
    return RemoteReturnMessage.class.getSimpleName() + ":" + callId + "," + ret + ", " + throwing;
  }
}
