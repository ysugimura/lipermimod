package com.cm55.lipermimod.call;

import java.io.*;
import java.util.*;
import java.util.stream.*;

import com.cm55.lipermimod.*;

/**
 * エクスポートされるオブジェクトのデスクリプション
 * <p>
 * 本システムでは、一方から一方に伝達されるオブジェクトの実体としては、それが実装する複数のインターフェースと
 * オブジェクトを識別するためのランダムな文字列のハンドルになる。
 * </p>
 * <p>
 * 例えば、サーバ側からクライアントに{@link ExportObjectDesc}が伝達された場合、クライアント側では、そのインターフェースを実装する
 * プロキシを作成し、クライアント側はプロキシのメソッドを呼び出すが、その呼出をサーバ側に伝えるためにハンドル、メソッド名、引数を指定する
 * といった具合である。
 * </p>
 * <p>
 * したがって、このケースの場合、サーバ側は、クライアント側にオブジェクトを伝える際に、そのインターフェースと識別文字列が格納された
 * {@link ExportObjectDesc}を転送することになる。
 * </p>
 * @author ysugimura
 */
public class ExportObjectDesc implements Serializable {
  private static final long serialVersionUID = 1;
  
  /** これはデバッグ用 */
  private transient Class<?>clazz;
  
  /** このオブジェクトが実装するすべての{@link IRemote}インターフェース名称 */
  private final String[]interfaces;

  /** ハンドル */
  public final RemoteHandle handle;
  
  /** 
   * このオブジェクトが実装するすべての{@link IRemote}インターフェース名称を指定する
   * @param intfClasses このオブジェクトが実装するすべての{@link IRemote}インターフェース名称の配列
   */
  public ExportObjectDesc(Class<?>clazz, Class<?>[]intfClasses) {
    this.clazz = clazz;
    interfaces = Arrays.stream(intfClasses).map(c->c.getName()).collect(Collectors.toList()).toArray(new String[0]);
    handle = new RemoteHandle();
  }
  
  /** 
   * このオブジェクトが実装するすべての{@link IRemote}インターフェース名称を取得する。
   * @return　このオブジェクトが実装するすべての{@link IRemote}インターフェース名称の配列
   */
  public String[]getInterfaces() {
    return interfaces;
  }

  @Override
  public String toString() {
    if (clazz != null) return ExportObjectDesc.class.getName() + ":" + clazz.getName();
    return ExportObjectDesc.class.getName() + ":" +
        Arrays.stream(interfaces).collect(Collectors.joining(","));
  }
  
  /*
   * 以下は特殊な用途
   * クライアントがデフォルトで保持するサーバ側のシステム・サービス用
   */
  
  /** システム・サービスのデスクリプション */
  public static ExportObjectDesc SYSTEM_SERVICE = new ExportObjectDesc(RemoteHandle.SYSTEM_SERVICE);

  /** システム・サービスのデスクリプション生成専用 */
  private ExportObjectDesc(RemoteHandle remoteHandle) {
    interfaces = new String[0];
    this.handle = remoteHandle;
  }
}
