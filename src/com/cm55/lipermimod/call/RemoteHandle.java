package com.cm55.lipermimod.call;

import java.io.*;

/**
 * リモートオブジェクトのハンドル。
 * <p>
 * リモートオブジェクトは、システム内において単一のランダムな文字列で識別される。
 * </p>
 * <p>
 * ただし、サーバに初めから存在するシステム・サービスの名称は"SYSTEM-SERVICE"としている。
 * </p>
 * @author ysugimura
 */
public class RemoteHandle implements Serializable {
  private static final long serialVersionUID = 1;

  /** 
   * ハンドルの値。UUID.randomUUID()によってランダムな文字列になる。
   */
  private final String value;

  /** ランダムな値のハンドルを作成する */
  public RemoteHandle() {
    value = java.util.UUID.randomUUID().toString();
  }
  
  @Override
  public int hashCode() {
    return value.hashCode();
  }
  
  @Override
  public boolean equals(Object obj) {    
    if (!(obj instanceof RemoteHandle)) return false;    
    RemoteHandle that = (RemoteHandle)obj;
    return this.value.equals(that.value);
  }

  @Override
  public String toString() {
    return value;
  }

  /** システム・サービス用のハンドル */
  public static final RemoteHandle SYSTEM_SERVICE = new RemoteHandle("SYSTEM-SERVICE");

  /** システム・サービスハンドル生成専用 */
  private RemoteHandle(String value) {
    this.value = value;
  }
}
