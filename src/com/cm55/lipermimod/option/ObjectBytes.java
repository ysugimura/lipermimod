package com.cm55.lipermimod.option;

import java.io.*;

import com.cm55.lipermimod.*;

/**
 * 
 * @author y.sugimura
 */
public class ObjectBytes {

  public static byte[] getBytes(Object object)  {
    try {
      ByteArrayOutputStream bout = new ByteArrayOutputStream();
      ObjectOutputStream oout = new ObjectOutputStream(bout);
      oout.writeObject(object);
      oout.close();
      return bout.toByteArray();
    } catch (NotSerializableException ex) {
      throw new LipeRMIException.NotSerializable(ex);
    } catch (IOException ex) {
      throw new InternalError();
    }
  }

  public static Object getObject(byte[] bytes) throws ClassNotFoundException {
    try {
      ByteArrayInputStream bin = new ByteArrayInputStream(bytes);
      ObjectInputStream oin = new ObjectInputStream(bin);
      return oin.readObject();
    } catch (IOException ex) {
      throw new InternalError();
    }
  }
}
