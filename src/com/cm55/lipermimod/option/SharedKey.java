package com.cm55.lipermimod.option;

import java.io.*;
import java.security.*;

import javax.crypto.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;

/**
 * 共有鍵暗号方式の鍵を保持するオブジェクト。
 * 共有鍵はクライアントにて作成され、サーバ側に渡されるが、その際にサーバ側の
 * 公開鍵にて暗号化される。サーバ側では秘密鍵にて共有鍵を復元する。
 * @author y.sugimura
 */
public class SharedKey implements Serializable {
  private static final long serialVersionUID = -825419221409181649L;

  private static final Log log = LogFactory.getLog(SharedKey.class);
  
  /** 暗号アルゴリズム */
  public final String algorithm;
  
  /** キービット数 */
  public final int keyBits;

  /** 平文の秘密鍵・送信しない(transient) */
  public transient SecretKey secretKey;
  
  /** 暗号化された秘密鍵 */
  public byte[]encrypted;
  
  @Override
  public String toString() {
    return algorithm + ", " + keyBits + ", " + secretKey + ", " + encrypted;
  }
  
  /** 共有鍵を生成 */
  public SharedKey(String algorithm, int keyBits) {
    this.algorithm = algorithm;
    this.keyBits = keyBits;
    try {
      KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
      SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
      keyGen.init(keyBits, random);
      secretKey = keyGen.generateKey();
    } catch (NoSuchAlgorithmException ex) {
      throw new LipeRMIException.Cipher(ex);
    }
  }

  /** 共有鍵でのオブジェクトの暗号化 */
  public byte[]encrypt(byte[]bytes) {
    if (log.isTraceEnabled()) log.trace("encrypt");
    
    try {
      Cipher cipher = Cipher.getInstance(algorithm);
      cipher.init(Cipher.ENCRYPT_MODE, secretKey);        
      return cipher.doFinal(bytes);
    } catch (Exception ex) {
      throw new LipeRMIException.Cipher(ex);
    }
  }
  
  /** 共有鍵でのオブジェクトの復号化 */
  public byte[] decrypt(byte[]bytes) {
    if (log.isTraceEnabled()) log.trace("decrypt");
    try {
      Cipher cipher = Cipher.getInstance(algorithm);
      cipher.init(Cipher.DECRYPT_MODE, secretKey);        
      return cipher.doFinal(bytes);
    } catch (Exception ex) {
      throw new LipeRMIException.Cipher(ex);
    }
  }
}
