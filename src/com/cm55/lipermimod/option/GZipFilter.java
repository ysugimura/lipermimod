/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod.option;

import java.io.*;
import java.util.zip.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;



/**
 * GZip filter to compact data using GZip I/O streams.
 * 
 * @author lipe
 * @date   07/10/2006
 * 
 * @see com.cm55.lipermimod.option.DefaultFilter
 */
public class GZipFilter extends DefaultFilter {
	
	public IRemoteMessage bytesToMessage(byte[]bytes) throws ClassNotFoundException {
	  ByteArrayOutputStream out = new ByteArrayOutputStream();
	  try (GZIPInputStream in = 
	      new GZIPInputStream(new ByteArrayInputStream(bytes))) {
	     int b;
	      while ((b = in.read()) != -1)
	        out.write(b);
	    return super.bytesToMessage(out.toByteArray());
	  } catch (ClassNotFoundException ex) {
	    throw ex;
	  } catch (Exception ex) {
	    throw new LipeRMIException.IOException(ex);
	  }

	}

	public byte[] messageToBytes(IRemoteMessage message) {
	  byte[]bytes = super.messageToBytes(message);
	  ByteArrayOutputStream out = new ByteArrayOutputStream();
	  try (OutputStream gzip = new GZIPOutputStream(out)) {
	    gzip.write(bytes);
	  } catch (IOException ex) {
	    throw new LipeRMIException.IOException(ex);
	  }
	  return out.toByteArray();
	}
}
