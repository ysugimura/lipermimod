package com.cm55.lipermimod;

import java.net.*;

/**
 * サーバ側では各々のクライアントとの接続、クライアント側ではサーバとの接続を表す。
 * @author ysugimura
 */
public interface Connection {
  public Socket getSocket();
  public void disconnect();
}
