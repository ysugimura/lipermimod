package com.cm55.lipermimod.exported;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

import com.cm55.lipermimod.*;

/**
 * あるオブジェクトについて、その{@link IRemote}インターフェースを取得する。同時にその他のチェックを行う。
 * 以下の条件がある。
 * <ul>
 * <li>オブジェクトは{@link Serializable}であってはならない。
 * <li>オブジェクトが実装する{@link IRemote}インターフェースは唯一でなくてはいけない。
 * <li>このインターフェースのメソッド名はすべて異なっていなければならない（Javaのメソッドオーバーロードは使用できないこととする）。
 * </ul>
 * <p>
 * 最後の条件の理由は、メソッドオーバーロードを有効にすると、メソッドを識別するために、引数型をも取り入れなければならないからである。
 * これは冗長であるほか、将来的にJSONによるシリアライゼーションを行う際にも面倒になってしまう。
 * </p>
 * @author ysugimura
 */
public class IRemoteExtractor {

  /**
   * 指定されたオブジェクトの{@link IRemote}及びその拡張インターフェースを取得する
   * @param object 対象とするオブジェクト
   * @return インターフェースクラス
   */
  static Set<Class<?>>extract(Object object) {
    
    //ystem.out.println("object " + object.getClass().getSuperclass());
    
    // このオブジェクトがSerializableであってはいけない。
    if (object instanceof Serializable) {
      throw new LipeRMIException.Incompatible("IRemote object should not be Serializable");
    }

    // このオブジェクトのクラスおよびスーパークラスの実装するすべてのIRemoteインターフェースを取得する
    Set<Class<?>>iremotes = new HashSet<>();
    Class<?>clazz = object.getClass();
    while (!clazz.equals(Object.class)) { 
      iremotes.addAll(
        Arrays.stream(clazz.getInterfaces())
        .filter(i->IRemote.class.isAssignableFrom(i))
        .collect(Collectors.toSet())
      );
      clazz = clazz.getSuperclass();
    }

    // これらの複数のインターフェースのメソッド名はすべて異なること。
    // メソッドオーバーロードはサポートしていない。
    checkMethodNameDuplication(iremotes);    
    return iremotes;
  }

  /**
   * オブジェクトの実装する直接のインターフェースだけではなく、上位階層も含めたすべてのインターフェースについて、
   * その単純メソッド名の重複が無いかを調べる。
   * また、第一引数がIFutureの場合の特殊なチェックも行う。
   * @param directInterfaces
   */
  private static void checkMethodNameDuplication(Set<Class<?>>directInterfaces) {

    // オブジェクトが直接実装しているインターフェースのみならず、インターフェース階層のすべてのインターフェースを取得する
    Set<Class<?>>allInterfaces = getAllInterfaces(directInterfaces);
    
    // RemoteHandleには記述しないが、チェックとしては、IUnrefrencedを必ず含める
    allInterfaces.add(IUnreferenced.class);
    
    StringBuilder error = new StringBuilder();
    
    Map<String, Class<?>>nameToIntf = new HashMap<>();    
    for (Class<?>intf: allInterfaces) {
      // すべてのインターフェースを取得したので、各々のインターフェースが定義しているメソッドのみを見る。つまり、getDeclaredMethods()
      Method[]methods = intf.getDeclaredMethods();
      for (Method method: methods) {
        futureCheck(method);
        String methodName = method.getName();
        //ystem.out.println("" + methodName);
        Class<?>previous = nameToIntf.get(methodName);
        if (previous != null) {
          error.append(methodName + " in " + previous.getName() + " and " + intf.getName());
        }
        nameToIntf.put(methodName, intf);
      }
    }

    if (error.length() > 0) {
      throw new LipeRMIException.Incompatible("Method overload not allowed:" + error.toString()); 
    }
  }
  
  /** 
   * すべてのインターフェースを返す。 
   * 例えば、
   * <pre>
   * class A extends IRemote, IUnreferenced {
   * }
   * class B implements IRemote, A {
   * }
   * </pre>
   * の場合、IRemote, IUnferenced, Aになる。
   * @param interfaceSet
   * @return
   */
  private static Set<Class<?>>getAllInterfaces(Set<Class<?>>interfaceSet) {
    Set<Class<?>>allSet = new HashSet<Class<?>>();
    for (Class<?>i: interfaceSet) {
      new Object() {
        void add(Class<?>i) {
          allSet.add(i);
          for (Class<?>c: i.getInterfaces()) add(c);
        }
      }.add(i);
    }
    return allSet;
  }
  
  /** 
   * IFutureは0番目のパラメータのみに使用可能。
   * また0番目の引数がIFutureの場合、IRemoteやSerializableではいけない 
   * この引数はリモートオブジェクトとして伝えられることも、シリアライズされることもない。
   */
  private static void futureCheck(Method method) {
    Class<?>[]types = method.getParameterTypes();
    if (types.length == 0) return;
    
    // 0番目以外にIFuture型があってはいけない
    for (int i = 1; i < types.length; i++) {
      if (IFuture.class.isAssignableFrom(types[i])) {
        throw new LipeRMIException.Incompatible("Can't use IFuture at " + i + "th parameter type");
      }
    }    
    
    Class<?>first = types[0];
    
    // 最初の引数がIFuture型であるか？
    if (!IFuture.class.isAssignableFrom(first)) return;    
    
    // 最初の引数がIFuture型の場合、Serializableであってはいけない。
    if (Serializable.class.isAssignableFrom(first) ||
        IRemote.class.isAssignableFrom(first)) {
      throw new LipeRMIException.Incompatible("IFuture should not be Serializable nor IRemote");
    }

    // IFutureの型パラメータを取得
    ParameterizedType firstType = (ParameterizedType)method.getGenericParameterTypes()[0];
    Class<?>firstActualType = (Class<?>)firstType.getActualTypeArguments()[0];
    
    // このメソッドの返り値と一致していること
    Class<?>returnType = getReferenceClass(method.getReturnType());
    if (returnType != firstActualType) {
      throw new LipeRMIException.Incompatible("IFuture parameter not matched with method return type");
    }
  }
  
  static Map<Class<?>, Class<?>>map = new HashMap<Class<?>, Class<?>>() {{
    put(byte.class, Byte.class);
    put(short.class, Short.class);
    put(int.class, Integer.class);
    put(long.class, Long.class);
    put(char.class, Character.class);
    put(float.class, Float.class);
    put(double.class, Double.class);
    put(boolean.class, Boolean.class);
  }};
  
  /**
   * 指定されたプリミティブ型に対応するオブジェクト型を取得する
   * @param input
   * @return
   */
  static Class<?>getReferenceClass(Class<?>input) {
    if (!input.isPrimitive()) return input;
    Class<?>clazz = map.get(input);
    if (clazz == null) throw new InternalError();
    return clazz;
  }
}
