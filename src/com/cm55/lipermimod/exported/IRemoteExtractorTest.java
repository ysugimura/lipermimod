package com.cm55.lipermimod.exported;

import static org.junit.Assert.*;

import java.io.*;
import java.util.*;

import org.junit.*;

import com.cm55.lipermimod.*;

public class IRemoteExtractorTest {

  @Test
  public void serializable() {
    try {
      IRemoteExtractor.extract(new Ser1());
      fail();
    } catch (LipeRMIException e) {}
    try {
      IRemoteExtractor.extract(new Ser3());
      fail();
    } catch (LipeRMIException e) {}
  }
  
  public interface Ser0 extends Serializable, IRemote {    
  }
  public static class Ser1 implements Ser0 {    
  }
  public interface Ser2 extends IRemote {    
  }
  public static class Ser3 implements Ser2, Serializable {    
  }
  
  @Test
  public void remoteIntf() {
    
    assertEquals(new HashSet<Class<?>>() {{
      add(Foo.class); add(Bar.class);
    }}, IRemoteExtractor.extract(new FooBar()));  
    
    assertEquals(new HashSet<Class<?>>() {{
      add(IRemote.class);
    }}, IRemoteExtractor.extract(new NoRemote()));  
    
  }
    
  public interface Foo extends IRemote {    
  }
  public interface Bar extends IRemote {    
  }
  public interface Sample {    
  }
  public static class FooBar implements Foo, Bar, Sample {    
  }
  public static class FooImpl implements Sample, Foo {    
  }
  public static class NoRemote implements IRemote {    
  }
  
  @Test
  public void overload() {    
    try {
      IRemoteExtractor.extract(new Ov2());
      fail();
    } catch (LipeRMIException e) {}
  }
  
  public interface Ov0 extends IRemote {    
    public void foo(String a);
  }
  public interface Ov1 extends Ov0 {
    public void foo(int a);
  }
  public static class Ov2 implements Ov1 {

    @Override
    public void foo(String a) {
      // TODO Auto-generated method stub
      
    }

    @Override
    public void foo(int a) {
      // TODO Auto-generated method stub
      
    }    
  }
  
  @Test
  public void superclass() {
    assertEquals(new HashSet<Class<?>>() {{
      add(Su0.class);
      add(Su1.class);
      add(Su2.class);
    }}, IRemoteExtractor.extract(new Su5()));
  }
  
  public interface Su0 extends IRemote {}
  public interface Su1 extends IRemote {}
  public interface Su2 extends IRemote {}
  public static class Su3 implements Su0,Su1 {}
  public static class Su4 extends Su3 implements Su0,Su2 {}
  public static class Su5 extends Su4 {}
  
  @Test
  public void Future() {
    IRemoteExtractor.extract(new Fu2());
    try {
      IRemoteExtractor.extract(new Fu3());
      fail();
    } catch (Exception ex) {}
  }
  
  public interface Fu0 extends IRemote {
    Integer foo(IFuture<Integer> f, String arg);
    int bar(IFuture<Integer>f, int value);
  }
  public interface Fu1 extends IRemote {
    Integer foo(String arg, IFuture<Integer>f);
  }
  public static class Fu2 implements Fu0 {

    @Override
    public Integer foo(IFuture<Integer> f, String arg) {
      return 0;
    }

    @Override
    public int bar(IFuture<Integer> f, int value) {
      // TODO Auto-generated method stub
      return 0;
    }    
  }
  public static class Fu3 implements Fu1 {
    @Override
    public Integer foo(String arg, IFuture<Integer> f) {
  
      return 0;
    }    
  }
}
