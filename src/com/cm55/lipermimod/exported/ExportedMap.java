package com.cm55.lipermimod.exported;

import java.util.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;

/**
 * エクスポートされたオブジェクトを保持するマップ。
 * <p>
 * エクスポートされたオブジェクトは{@link Exported}として表現されるが、
 * ここではオブジェクトそのもの、および{@link RemoteHandle}から、{@link Exported}を素早く取得できるように
 * 二つのマップを作成する。
 * この二つのマップへの登録・削除は同時に行われる。片方のみが残るということは無い。
 * </p>
 * @author ysugimura
 */
class ExportedMap {

  /** 任意のオブジェクト/{@link Exported}マップ */
  protected Map<ObjectKey, Exported>exportedObjects = new HashMap<ObjectKey, Exported>();
  
  /** {@link RemoteHandle}/{@link Exported}のマップ */
  private Map<RemoteHandle, Exported>remoteToExports = 
    new HashMap<RemoteHandle, Exported>();

  /**
   * 新たな{@link Exported}を登録する。登録済のものであれば何もしない
   * @param exported {@link Exxported}オブジェクト
   */
  void put(Exported exported) {
    exportedObjects.put(new ObjectKey(exported.object), exported);
    remoteToExports.put(exported.objectDesc.handle, exported);  
  }
  
  /**
   * 任意のオブジェクトから対応する登録済の{@link Exported}を取得する。まだ存在しない場合はnullを返す。
   * @param object 任意のオブジェクト
   * @return 対応する{@link Exported}もしくはnull。
   */
  <T extends IRemote>Exported get(T object) {
    return exportedObjects.get(new ObjectKey(object));
  }
  
  /**
   * {@link RemoteHandle}から対応する{@link Exported}を取得する。
   * 存在しない場合はnullを返す。
   * @param remoteHandle {@link RemoteHandle}
   * @return
   */
  Exported get(RemoteHandle remoteHandle) {
    return remoteToExports.get(remoteHandle);
  }
  
  void remove(Exported e) {
    exportedObjects.remove(new ObjectKey(e.object));
    remoteToExports.remove(e.objectDesc.handle);
  }
  
  void remove(RemoteHandle remoteHandle) {
    Exported e = remoteToExports.remove(remoteHandle);
    if (e != null) exportedObjects.remove(new ObjectKey(e.object));
  }

  Exported[]all() {
    return exportedObjects.values().toArray(new Exported[0]);
  }
  
  /** 
   * オブジェクトを{@link HashMap}で識別するためのキー
   * <p>
   * 同一オブジェクトを{@link HashMap}で識別するためには、特に同一比較をequalsではなく==で行わなければいけない。
   * そのため、任意のオブジェクトの同一性を保証するため、このオブジェクトでラップし、{@link #equals(Object)}を
   * オブジェクト自身の==にする。
   * hashCodeは何でも良いのだが、System.identityHashCodeが適当
   * </p>
   */
  private static class ObjectKey {
    private IRemote object;    
    
    private <T extends IRemote>ObjectKey(T object) {
      this.object = object;
    }
    
    @Override
    public int hashCode() {
      return System.identityHashCode(object);
    }
    
    @Override 
    public boolean equals(Object o) {
      if (!(o instanceof ObjectKey)) return false;
      return this.object == ((ObjectKey)o).object;
    }
  }
}
