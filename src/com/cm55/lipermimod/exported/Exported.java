package com.cm55.lipermimod.exported;

import java.util.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;

/** 
 * エクスポートオブジェクト情報
 * <p>
 * {@link IRemote}インターフェースを実装するローカル側のオブジェクトがリモート側に転送される際には、転送されたという事実が
 * ローカル側の{@link ExportedMap}に記録される。
 * </p>
 * <p>
 * 
 * </p>
 * @author ysugimura
 */
public class Exported {
  
  /** エクスポートされたオブジェクト */
  public final IRemote object;
  
  /** RemoteHandle */
  public final ExportObjectDesc objectDesc;

  /** このオブジェクトを参照しているコネクションIDのセット */
  private Set<Long>referrers = new HashSet<Long>();

  /** グローバルフラグ。このフラグONの場合はクライアントの接続数に関わらず保持されなければいけない */
  private boolean global;
  
  /** エクスポートオブジェクトを作成 */
  <T extends IRemote>Exported(T object, Class<?>[]intfClasses) {
    assert object != null;
    this.object = object;
    objectDesc = new ExportObjectDesc(object.getClass(), intfClasses);
  }

  /** 
   * グローバルフラグを設定する
   * @param value true：参照数が0になっても保持する。false:参照数が０になったら保持しない
   */
  public synchronized void setGlobal(boolean value) {
    global = value;
  }

  /** 指定コネクションからの参照を追加 */
  synchronized void addReferenceFrom(long connId) {
    referrers.add(connId);
  }
  
  /** 
   * 指定コネクションからの参照を削除。
   */
  synchronized void removeReferenceFrom(long connId) {
    referrers.remove(connId);
  }
  
  /**
   * クライアントからの参照があるか？
   * グローバルの場合は、参照があるものとする。
   * @return true：参照がある。false：参照がない
   */
  public synchronized boolean hasReference() {
    return referrers.size() > 0 || global;
  }
  
  // 以下はシステム・サービス専用 //////////////////////////////////////////////////
  
  /** システム・サービス専用 */
  static <T extends IRemote>Exported createSystemServiceExported(T object) {
    return new Exported(object, ExportObjectDesc.SYSTEM_SERVICE);
  }
    
  /**　システム・サービス専用 */
  private <T extends IRemote> Exported(T object, ExportObjectDesc objectDesc) {
    assert object != null;
    this.object = object;
    this.objectDesc = objectDesc;
  }
}
