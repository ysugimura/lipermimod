package com.cm55.lipermimod.exported;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;

public class ExportedObjects {

  protected static final Log log = LogFactory.getLog(ExportedObjects.class);
  
  private ExportedMap map = new ExportedMap();
  
  /**
   * 指定されたオブジェクトをエクスポート
   * @param object
   * @return
   */
  public synchronized <T extends IRemote>Exported ensureExported(T object) {
    Exported exportedObject = map.get(object);
    if (exportedObject != null) return exportedObject;
    
    Class<?>[]intfClasses = IRemoteExtractor.extract(object).toArray(new Class[0]);
    exportedObject = new Exported(object, intfClasses);
    map.put(exportedObject);
    return exportedObject;
  }
  
  /** 
   * 指定されたオブジェクトがエクスポート済みであればそのRemoteHandleを返す。
   * <p>
   * そうではなく、もしtypeがIRemoteであればエクスポートして、RemoteHandleを返す。
   * いずれの場合もconnectionへの参照を追加する。
   * </p>
   * 
   * @param object 返り値あるいは引数オブジェクト
   * @param connection ConnectionHandler
   */
  public synchronized <T extends IRemote>Exported ensureExportedWithRef(T object, long connId) {

    // このオブジェクトをエクスポートする
    Exported exportedObject = ensureExported(object);    
    exportedObject.addReferenceFrom(connId);
    return exportedObject;
  }

  /** 
   * RemoteHandleに対応をするオブジェクトを取得する。
   * 存在しなければnullを返す。
   * 存在した場合はconnectionへの参照を追加する。
   * @param remoteHandle
   * @param connection
   * 
   * @return オブジェクト
   */
  @SuppressWarnings("unchecked")
  public synchronized <T extends IRemote>T getFromRemoteHandle(RemoteHandle remoteHandle, 
      long connId) {
    if (log.isTraceEnabled()) log.trace("get " + remoteHandle);    
    Exported exportedObject = map.get(remoteHandle);    
    if (exportedObject == null) return null;
    exportedObject.addReferenceFrom(connId);
    return (T)exportedObject.object;      
  }

  /** 指定されたオブジェクトのunreferencedが呼び出された */
  public synchronized <T extends IRemote>void unreferencedFrom(T object, long connId) {
    if (log.isTraceEnabled()) log.trace("unreferenced");
    Exported exported = map.get(object);
    if (exported == null) return;
    exported.removeReferenceFrom(connId);
    if (!exported.hasReference()) removeExported(exported);
  }
  
  /** 
   * 指定されたコネクションが切断した。
   * エクスポート中のオブジェクトから、このコネクションへの参照を減じる。 */
  public synchronized void connectionClosed(long connId) {
    for (Exported exported: map.all()) {      
      exported.removeReferenceFrom(connId);
      if (!exported.hasReference()) removeExported(exported);
    }        
  }

  /** 指定されたExportedObjectを登録削除する。その前にunreferenced()を呼ぶ */
  public synchronized void removeExported(Exported exported) {
    if (log.isTraceEnabled()) log.trace("removeExported " + exported);
    if (exported.object instanceof IUnreferenced) {
      ((IUnreferenced)exported.object).unreferenced();
    }
    map.remove(exported);
  }


  /**
   * これはサーバからのみ呼び出される。サーバ側は最初に{@link SystemService}オブジェクトをエクスポートしなければならない。
   * そうでなければ、クライアント側から{@link SystemService#getGlobal(String)}を呼び出すことさえできない。
   * このための{@link RemoteHandle}はあらかじめ決定済である。
   * @param object
   * @return
   */
  public synchronized <T extends IRemote>Exported registerSystemServiceExported(T object) {
    Exported exportedObject = Exported.createSystemServiceExported(object);
    map.put(exportedObject);
    return exportedObject;
  }

}