package com.cm55.lipermimod.util;

import java.util.concurrent.*;

/**
 * 
 * @author y.sugimura
 */
public class ThreadPool {
  
  private static ExecutorService ex;
  
  public static <T> Future<T> submit(Callable<T> callable) {
    ensureService();
    return ex.submit(callable);    
  }
  
  public static Future<?> submit(Runnable runnable) {
    ensureService();
    return ex.submit(runnable);
  }
  
  private static synchronized void ensureService() {
    if (ex != null) return;
    ex = Executors.newCachedThreadPool(
        new ThreadFactory() {
          public Thread newThread(Runnable r) {
            Thread thread = new Thread(r);
            thread.setDaemon(true);
            return thread;
          }
        }
    );
  }
}
