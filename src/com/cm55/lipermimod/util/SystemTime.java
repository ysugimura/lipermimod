package com.cm55.lipermimod.util;

/**
 * System.currentTimeMillis()を呼び出す
 * @author ysugimura
 */
public interface SystemTime {

  public static long get() { return System.currentTimeMillis(); }
  
}
