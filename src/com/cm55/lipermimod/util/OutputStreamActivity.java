package com.cm55.lipermimod.util;

import java.io.*;

/**
 * 大きな出力ストリームを処理中に、画面表示等が完全にストップしてしまうのを避けるため
 * ここを通過したバイト数を｛@link #activity(int)}によって報告する。
 * @author ysugimura
 */
public abstract class OutputStreamActivity extends OutputStream {
  private OutputStream out;
  
  public OutputStreamActivity(OutputStream out) {
    this.out = out;
  }
  
  public void write(int b) throws IOException {
    out.write(b);
    activity(1);
  }
  public void write(byte b[]) throws IOException {
    out.write(b);
    activity(b.length);
 }    
  public void write(byte b[], int off, int len) throws IOException {
    out.write(b, off, len);
    activity(len);
  }
  public void flush() throws IOException {
    out.flush();
  }
  public void close() throws IOException {
    out.close();
  }
  protected abstract void activity(int size);
}