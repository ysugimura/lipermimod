package com.cm55.lipermimod.util;

import java.io.*;

/**
 * 大きな入力ストリームを処理中に、画面表示等が完全にストップしてしまうのを避けるため
 * ここを通過したバイト数を｛@link #activity(int)}によって報告する。
 * @author ysugimura
 */
public abstract class InputStreamActivity extends InputStream {
  private InputStream in;
  public InputStreamActivity(InputStream in) {
    this.in = in;
  }
  public int read() throws IOException {
    int r = in.read();
    if (r != -1) activity(1);
    return r;
  }
  public int read(byte b[]) throws IOException {
    int size = in.read(b);
    if (size >= 0) activity(size);
    return size;
  }
  public int read(byte b[], int off, int len) throws IOException {
    int size = in.read(b, off, len);
    if (size >= 0) activity(size);
    return size;
  }
  public long skip(long n) throws IOException {
    return in.skip(n);
  }
  public int available() throws IOException { return in.available(); }
  public void close() throws IOException { in.close(); }
  public synchronized void mark(int readlimit) { in.mark(readlimit); }
  public synchronized void reset() throws IOException { in.reset(); }
  public boolean markSupported() { return in.markSupported(); }
  protected abstract void activity(int size);
}
