package com.cm55.lipermimod;

import org.junit.*;
import static org.junit.Assert.*;

public class HeartBeatTest {


  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    server.registerGlobal("serverGlobal",  new ServerGlobalImpl());
    client = new Client();
  }
  
  @Test
  public void test1() throws Exception {    
    // サーバ側
    server.setHeartBeatTimeout(2);
    server.bind(4455);

    // クライアント側
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");    
    Thread.sleep(5000);
    try {
      serverGlobal.hello();
      fail();
    } catch (Exception ex) {      
    }
    server.close();
  }
  
  @Test
  public void test2() throws Exception {    
    // サーバ側
    server.setHeartBeatTimeout(2);
    server.bind(4455);

    // クライアント側
    client.setHeartBeatInterval(1);
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");    
    Thread.sleep(5000);
    serverGlobal.hello();
    server.close();
  }
  
  
  public static interface ServerGlobal extends IRemote {
    public void hello();
  }
  
  private static class ServerGlobalImpl implements ServerGlobal {
    public void hello() {

    }
  }
  


}
