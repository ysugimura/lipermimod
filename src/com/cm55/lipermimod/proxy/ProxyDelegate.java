/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod.proxy;

import java.lang.reflect.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.handler.*;


/**
 * ローカルプロキシの呼び出しハンドラ
 * <p>
 * リモート側のオブジェクトの代わりとしてローカル側に作成されたプロキシに対してローカルからの呼び出しが発生した場合には、
 * その呼出をリモート側の呼び出しにしなければならない。
 * このために、ここではJavaの{@link Proxy}への呼び出しを処理する{@link InvocationHandler}を実装し、
 * プロキシへの呼び出しが起こったら、リモートとのコネクション{@link ConnectionImpl}を呼び出す。
 * </p>
 * <p>
 * また、プロキシには特別なインターフェース{@link ProxyInternal}が実装してあり、このメソッドを呼び出すことにより
 * ローカル側の情報を取得することが可能になる。
 * </p>
 */
class ProxyDelegate implements InvocationHandler  {

  private static final Log log = LogFactory.getLog(ProxyDelegate.class);

  private final RemoteHandle ｈandle;
	private final RemoteInvoker invoker;
	
	
	ProxyDelegate(RemoteHandle handle, RemoteInvoker invoker) {
	  this.ｈandle = handle;
		this.invoker = invoker;
	}
  
	/**
	 * このメソッドはJavaの{@link Proxy}の処理として呼び出される。つまり、プロキシのメソッドが呼ばれた場合、
	 * どのメソッドが呼び出されても、このメソッドが処理を代行することになる。
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
	  if (log.isTraceEnabled()) log.trace("invoke " + method);	  
	  	   
	  // プロキシ内部のローカル情報を返す。
	  if (method.getName().equals(ProxyInternal.GET_REMOTE_HANDLE)) {
      return ｈandle;
    }
	  /*
	  else if (method.equals(ProxyInternal.GET_CONNECTION_IMPL)) {
      return conn;
    }
    */
	  
	  // リモートのメソッド呼び出しを行う
	  return invoker.invokeRemoteMethod(ｈandle, method, args);
	}
}
