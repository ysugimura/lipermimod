package com.cm55.lipermimod.proxy;

import com.cm55.lipermimod.call.*;

/**
 * プロキシを作成するとき、本来のインターフェース（IRemoteのサブインターフェース）
 * と同時に、このインターフェースを実装させる。
 * これにより、プロキシがゴミになったタイミングでfinalizeメソッドが呼び出される。
 */
public interface ProxyInternal {
  public static final String GET_REMOTE_HANDLE = "lipermimod_getRemoteHandle";
  //public static final String GET_CONNECTION_IMPL = "lipermimod_getConnectionImpl";
  
  public RemoteHandle lipermimod_getRemoteHandle();
  //public ConnectionImpl lipermimod_getConnectionImpl();
}

