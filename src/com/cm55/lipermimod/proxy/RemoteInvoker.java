package com.cm55.lipermimod.proxy;

import java.lang.reflect.*;

import com.cm55.lipermimod.call.*;

/**
 * プロキシが呼び出された場合に、実際にリモート呼び出しを行うためのインターフェース
 * @author ysugimura
 */
public interface RemoteInvoker {

  /**
   * リモートオブジェクトのメソッドを呼び出す
   * @param handle リモートオブジェクトのハンドル
   * @param method 呼び出すメソッド
   * @param args 引数
   * @return メソッドの返り値
   * @throws Exception
   */
  Object invokeRemoteMethod(RemoteHandle handle, 
      Method method, Object[] args) throws Exception;

  /**
   * リモートオブジェクトのunreferencedメソッドを呼び出す
   * @param handle
   * @throws Exception
   */
  void invokeRemoteUnreferenced(RemoteHandle handle) throws Exception;
  
}
