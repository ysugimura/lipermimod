/**
 * リモートにあるオブジェクトについて、そのメソッドを、あたかもローカルにあるかのように呼び出せるためには、ローカル側にJavaの{@link Proxy}を作成し、
 * それをリモートオブジェクトの代わりとしてローカル側システムに提供する。
 * 
 * 基本的には、Javaの{@link Proxy}の仕組みを用いて、これを行うのだが、特筆すべきこととしては三点ある。
 * <ul>
 * <li>メソッドが呼び出されたら、実際のリモート呼び出しを行う。これを{@link RemoteInvoker}に呼び出しを依頼することによって行う。
 * <li>ローカル側でオブジェクトへの参照が無くなったら、リモート側にそれを伝えるべきである。これを{@link RemoteInvoker}にその機能がある。
 * <li>{@link Proxy}の作成は{@link ProxyObjectsImpl}で行い、作成済プロキシを保持するが、これは{@link WeakReferece}として保持されなければ
 * ならない。強参照ではいけない。
 * </ul>
 * @author ysugimura
 */
package com.cm55.lipermimod.proxy;