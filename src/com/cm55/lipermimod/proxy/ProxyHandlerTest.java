package com.cm55.lipermimod.proxy;

import java.lang.reflect.*;

import org.junit.*;

public class ProxyHandlerTest {

  public interface Foo {
    public void foo(String a);
    public void bar(String a, String  b);
  }
  @Test
  public void test() {
    
     Foo foo = (Foo)Proxy.newProxyInstance(ProxyHandlerTest.class.getClassLoader(), 
       new Class[] { Foo.class },
       new InvocationHandler() {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
          // TODO Auto-generated method stub
          //ystem.out.println("" + method
          //+ "," +  Arrays.stream(args).map(a->a.toString()).collect(Collectors.joining(",")));
          return null;
        }
       }
     );
     
     foo.bar("a", "b");
  }

}
