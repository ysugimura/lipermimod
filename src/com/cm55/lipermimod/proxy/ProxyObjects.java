package com.cm55.lipermimod.proxy;

import java.lang.ref.*;
import java.lang.reflect.Proxy;
import java.net.*;
import java.util.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;

/**
 * リモートオブジェクトの代わりにローカル側に作成されるプロキシの集合
 * <p>
 * リモート側のオブジェクトをローカルで受け取る場合としては、以下の二種類がある。例えば、ローカル側がクライアントの場合には、
 * </p>
 * <ul>
 * <li>サーバ側からコールバックをウケた時の引数として与えられる。
 * <li>サーバ側のメソッド呼び出しの返り値として送られる。
 * </ul>
 * <p>
 * このとき、実際にローカル側にやってくるものは、サーバ側の実際のオブジェクトではなく、{@link ExportObjectDesc}オブジェクトになる。
 * ここには、もともとのサーバ側オブジェクトの実装する複数のインターフェースと、オブジェクト識別子{@link RemoteHandle}が含まれている。
 * </p>
 * <p>
 * ローカル側では、このオブジェクトに対するメソッド呼び出しを行うわけだが、上記はただのデータなので、そのままでは当然呼び出しはできない。
 * これを行うには、ローカル側に上記のインターフェースすべてを実装するプロキシを作成する。このプロキシのメソッドを呼び出すことによって、
 * 実際にはサーバ側への通信を行うことになる。
 * </p>
 * <p>
 * したがって、作成されるプロキシの「中身」としては、サーバとの通信を行うものになるが、これは{@link ProxyDelegate}が担っている。
 * </p>
 * <h2>実装上の注意</h2>
 * <p>
 * ここでのプロキシオブジェクトへの参照は弱参照でなければならない。そうでないと、プロキシが不要になったときに回収されなくなる。
 * ローカル内でプロキシオブジェクトが使用されている限りは参照が保持されるが、ローカルにてこれが不要になったときは、リモート側に
 * その旨を伝えなければならない。これによって、サーバ側オブジェクトも回収操作が可能になる。
 * </p>
 */
public class ProxyObjects {

  private static final Log log = LogFactory.getLog(ProxyObjects.class);
  
  /** 
   * プロキシを弱参照で保持するオブジェクト。
   * プロキシに対しては弱参照であるが、RemoteHandleは強参照で保持する。
   * プロキシに対する参照がなくなると、このオブジェクト自体がrefQueueに
   * 格納される。 */
  private class ProxyHolder extends WeakReference<Object> {
    private ExportObjectDesc objectDesc;
    private RemoteHandle remoteHandle;      
    private RemoteInvoker conn;
    private ProxyHolder(Object proxy, ExportObjectDesc objectDesc, RemoteInvoker connectionHandler) {
      super(proxy, unrefQueue);
      this.objectDesc = objectDesc;
      this.remoteHandle = objectDesc.handle;
      this.conn = connectionHandler;
    }     
  }
  
  
  /** RemoteHandleとそのプロキシのマップ */
  private Map<RemoteHandle, ProxyHolder> proxyObjectMap = 
    Collections.synchronizedMap(new HashMap<RemoteHandle, ProxyHolder>());

  /** 参照されなくなったプロキシを格納していたProxyHolderオブジェクトが
   * 格納されるキュー
   */
  private ReferenceQueue<Object>unrefQueue = new ReferenceQueue<Object>();

  private RemoteInvoker remoteInvoker;
  

  /** 
   * このオブジェクトを作成する。同時にunrefQueueの回収スレッドを動作させる
   */
  public void setup(RemoteInvoker remoteInvoker) {
    this.remoteInvoker = remoteInvoker;
    new Thread(this::garbageCollect).start();    
  }
  
  /**
   * {@link #unrefQueue}の回収スレッド
   * ローカル側で不要になったプロキシについて、マップから削除する。
   * 同時にリモート側のunreferencedメソッドを呼び出す。
   * これはオブジェクトが{@link IUnreferenced}を未実装であっても行われる。
   * 詳細は{@link IUnreferenced}を参照のこと。
   */
  private void garbageCollect() {
    while (true) {
      ProxyHolder proxyHolder;
      try {
        proxyHolder = (ProxyHolder)unrefQueue.remove();
      } catch (InterruptedException ex) {
        break;
      }          
      proxyObjectMap.remove(proxyHolder.remoteHandle);  
      try {
        // unreferencedメソッドを呼び出す
        proxyHolder.conn.invokeRemoteUnreferenced(
          proxyHolder.remoteHandle);
      } catch (SocketException ex) {
        // 既に切断されているがために書き込めない。基本的に無視する。
        if (log.isTraceEnabled()) 
          log.trace("Could not call unreferenced to disconnected socket " + proxyHolder.objectDesc);
      } catch (Exception ex) {
        log.warn("Could not call unreferenced for " + proxyHolder.objectDesc, ex);
      }
    }    
  }
  
  /** 
   * {@link RemoteHandle}で記述されるリモートに存在するオブジェクトの、ローカル側の操作対象としてのプロキシオブジェクトを取得する。
   * 存在しなければ作成する。ローカル側では、このプロキシオブジェクトを操作することにより、操作内容がリモートに伝えられる。
   * 
   * @param objectDesc リモートに存在するオブジェクトの記述子
   * @return ローカルでのプロキシオブジェクト
   */
  public synchronized Object ensureProxyForObjectDesc(
      ExportObjectDesc objectDesc) {    
    assert objectDesc != null;    
    
    if (log.isTraceEnabled()) log.trace("ensureProxyObjectForRemoteHandle " + objectDesc);
    
    // マップからプロキシオブジェクトを取得してみる。
    Object proxyObject = null;
    ProxyHolder proxyHolder = proxyObjectMap.get(objectDesc.handle);
    if (proxyHolder != null) {
      proxyObject = proxyHolder.get();
    }
    
    // プロキシオブジェクトが存在しない場合、作成してマップに登録
    if (proxyObject == null) {
      try {
        ClassLoader classLoader = ProxyObjects.class.getClassLoader();
        List<Class<?>>classes = new ArrayList<>();
        for (String name: objectDesc.getInterfaces()) {
          try {
          classes.add(Class.forName(name));
          } catch (ClassNotFoundException ex) {
            log.error("Class not found:" + name, ex);
            throw ex;
          }
        }
        classes.add(ProxyInternal.class);
        proxyObject = Proxy.newProxyInstance(
          classLoader,
          classes.toArray(new Class[0]),
          new ProxyDelegate(objectDesc.handle, remoteInvoker)
        );
      } catch (ClassNotFoundException e) {
        
      }
      proxyObjectMap.put(objectDesc.handle, new ProxyHolder(proxyObject, objectDesc, remoteInvoker));
    }
    
    return proxyObject; 
  }  
  
  /**
   * クライアント用にシステム・サービスプロキシオブジェクトを作成する
   * @return システム・サービスプロキシオブジェクト
   */
  public synchronized SystemService createSystemService() {
    return (SystemService)Proxy.newProxyInstance(
      SystemService.class.getClassLoader(), 
      new Class[] { SystemService.class }, 
      new ProxyDelegate(RemoteHandle.SYSTEM_SERVICE, remoteInvoker)
    );
  }


}