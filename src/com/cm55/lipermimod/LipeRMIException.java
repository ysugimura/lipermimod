/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod;

import java.io.*;

/**
 * General LipeRMI exception 
 * 
 * @author lipe
 * @author y.sugimura
 * @date   05/10/2006
 */
public abstract class LipeRMIException extends RuntimeException {

	private static final long serialVersionUID = 7324141364282347199L;

	protected LipeRMIException() {
		super();
	}

	protected LipeRMIException(String message, Throwable cause) {
		super(message, cause);
	}

	protected LipeRMIException(String message) {
		super(message);
	}

	protected LipeRMIException(Throwable cause) {
		super(cause);
	}
	
	/** 未知のエラー */
	public static class Unknown extends LipeRMIException {
    private static final long serialVersionUID = -9205661990952085013L;
    public Unknown(String message) { super(message); }
	}
	
	 /** 切断 */
  public static class Disconnected extends LipeRMIException {
    private static final long serialVersionUID = 1L;
    public Disconnected(String message) { super(message); }
    public Disconnected(Throwable cause) { super(cause); }
  }
  
	/** I/Oエラー */
	public static class IOException extends LipeRMIException {
    private static final long serialVersionUID = 7119492797097812977L;
    public IOException(String message) { super(message); }
	  public IOException(Throwable cause) { super(cause); }
	}
	
	/** Serializableでない */
	public static class NotSerializable extends LipeRMIException {
    private static final long serialVersionUID = -5435485267664488616L;
	  
    public NotSerializable(NotSerializableException ex) {
      super(ex);
    }
	}
	
	/** 実装オブジェクトが見つからない */
	public static class NoImplementation extends LipeRMIException {
    private static final long serialVersionUID = -6411325953700702830L;
    public NoImplementation(String message) { super(message); }
	}
	
	/** メソッドが見つからない */
	public static class NoSuchMethod extends LipeRMIException {
    private static final long serialVersionUID = 2575814357410760954L;
    public NoSuchMethod(String message) { super(message); }
	}
	
	/** クラスが見つからない */
	public static class ClassNotFound extends LipeRMIException {
    private static final long serialVersionUID = 2930380575853547740L;
    public ClassNotFound(String message) { super(message); }
	}
	
	/** 互換性の無いクラス・インターフェース */
	public static class Incompatible extends LipeRMIException {
    private static final long serialVersionUID = 1300131635738015949L;
	  public Incompatible(String message) { super(message); }
	}
	
	/** リモート側でのエラー */
	public static class RemoteError extends LipeRMIException {
    private static final long serialVersionUID = 4386615015486317057L;
	  public RemoteError(Throwable th) { super(th); }
	}
	
	/** 暗号機能エラー */
	public static class Cipher extends LipeRMIException {
    private static final long serialVersionUID = -4539214924490556114L;
	  public Cipher(Throwable th) { super(th); }
	}
}
