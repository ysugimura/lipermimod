package com.cm55.lipermimod.server;

import java.io.*;
import java.util.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.handler.*;
import com.cm55.lipermimod.util.*;

/**
 * 生存中のクライアントのコネクションハンドラを保持する。
 * addClientHandler/removeClientHandlerでコネクションハンドラを追加・削除する。
 */
public class ClientStock  {
  
  private static final Log log = LogFactory.getLog(ClientStock.class);
  
  /** ハートビートタイムアウト秒数 */
  int heartBeatTimeout = 0;
  
  private List<ConnectionImpl>handlers = new ArrayList<ConnectionImpl>();
  
  private List<IServerListener> listeners = new LinkedList<IServerListener>();
  
  private DetectThread detectThread;
  
  public void setHeartBeatTimeout(int value) {
    this.heartBeatTimeout = value;
  }
  
  /**
   * このシステムのユーザ向けのリスナーを登録する
   * @param listener
   */
  public synchronized void addListener(IServerListener listener) {
    listeners.add(listener);
  }

  /**
   * このシステムのユーザ向けのリスナーを削除する
   * @param listener
   */
  public synchronized void removeListener(IServerListener listener) {
    listeners.remove(listener);
  }
  
  /** 
   * サーバにクライアントが接続してきたため、それを処理するコネクションハンドラを登録する。
   * これ以降、このハンドラの最後の通信時間を調べて、ハートビート時間を超えていたら切断されたとみなす
   * @param clientHandler
   */
  public synchronized void addClientHandler(ConnectionImpl clientHandler) {
    if (log.isTraceEnabled()) log.trace("addClientHandler " + clientHandler);
    handlers.add(clientHandler);     
    for (IServerListener listener : listeners)
      listener.clientConnected(clientHandler);
  }
  
  /** 
   * クライアントとのコネクションが閉じられたため、監視を中止する
   * @param clientHandler
   */
  public synchronized void removeClientHandler(ConnectionImpl clientHandler) {
    if (log.isTraceEnabled()) log.trace("removeClientHandler clientHandler");
    handlers.remove(clientHandler);
    for (IServerListener listener : listeners) {
      listener.clientDisconnected(clientHandler);
    }
  }

  /** 
   * クライアントの接続監視を開始する
   */
  public synchronized void startDisconnectDetection() {
    if (detectThread != null) throw new IllegalStateException();
    (detectThread = new DetectThread()).start();
  }

  /** クライアントの接続監視を終了する */
  public void interruptDisconnectDetection() {
    detectThread.interrupt();
  }
  
  class DetectThread extends Thread {
    /**
     * 全クライアントをチェックし、KeepAliveTimeout秒数以上の間メッセージの無い クライアントは切断されているものとみなす
     */
    public void run() {

      if (log.isTraceEnabled()) log.trace("starting keep-alive checking... " + heartBeatTimeout);

      if (heartBeatTimeout == 0)
        return;

      while (!isInterrupted()) {
        try {
          Thread.sleep(heartBeatTimeout * 1000);
        } catch (InterruptedException ex) {
          break;
        }
        // long currentTime = System.currentTimeMillis();
        long currentTime = SystemTime.get();
        if (log.isTraceEnabled()) log.trace("currentTime:" + currentTime);

        ConnectionImpl[] handlerArray;
        synchronized (ClientStock.this) {
          handlerArray = handlers.toArray(new ConnectionImpl[0]);
        }

        for (ConnectionImpl handler : handlerArray) {
          if (currentTime - handler.getLastIOTime() <= heartBeatTimeout * 1000) {
            continue;
          }

          if (log.isTraceEnabled()) log.trace("client timeout " + handler);

          // 切断されている
          try {
            handler.getSocket().close();
          } catch (IOException ex) {
          }
        }

      }
    }
  }
}