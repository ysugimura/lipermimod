package com.cm55.lipermimod.server;

import com.cm55.lipermimod.call.*;

/**
 * {@link SystemService}の実装
 * 詳細は{@link SystemService}を参照のこと
 * @author ysugimura
 */
public class SystemServiceImpl implements SystemService {

  private final GlobalExports globalExports;
  
  public SystemServiceImpl(GlobalExports globalExports) {
    this.globalExports = globalExports;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T getGlobal(String globalName) {
    return (T)globalExports.getGlobal(globalName);
  }

  /** 何もしなくてよい。通信が発生すればよい */
  @Override
  public void heartBeat() {
 
  }

}
