package com.cm55.lipermimod.server;

import java.util.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.exported.*;

/**
 * サーバ側においてグローバル名称を与えられている{@link Exported}のマップ
 * @author ysugimura
 */
public class GlobalExports {

  protected static final Log log = LogFactory.getLog(GlobalExports.class);
  
  private ExportedObjects exportedObjects;
  private Map<String, Exported>globals = new HashMap<>();
  
  public GlobalExports(ExportedObjects exportedObjects) {
    this.exportedObjects = exportedObjects;
  }
  
  /** グローバルとしてエクスポート */
  public synchronized void registerGlobal(String globalName, IRemote object) {
    
    if (log.isTraceEnabled()) log.trace("registerGlobal " + globalName + ", " + object );    
        
    // 指定されたオブジェクトをエクスポートする。オブジェクトに異常があれば、ここで例外が発生
    Exported exported = exportedObjects.ensureExported(object);
    exported.setGlobal(true);
    
    Exported previous = globals.get(globalName);
    if (previous != null) previous.setGlobal(false);
    
    // マップに登録
    globals.put(globalName, exported);
  }
  
  /** 名称のグローバル登録を削除する */
  public synchronized void unregisterGlobal(String globalName) {
    Exported exported = globals.remove(globalName);
    if (exported == null) return;
    exported.setGlobal(false);
    if (!exported.hasReference()) {
      exportedObjects.removeExported(exported);
    }
  }

  public synchronized Object getGlobal(String globalName) {
    Exported exported = globals.get(globalName);
    if (exported == null) return null;
    return exported.object;
  }
}
