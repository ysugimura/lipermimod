package com.cm55.lipermimod.handler;

import com.cm55.lipermimod.exported.*;
import com.cm55.lipermimod.proxy.*;

/**
 * 一つのコネクションについて様々な処理を協調させて行うために必要なデータ
 * @author ysugimura
 */
public class ConnEnv {

  /** コネクション番号 */
  final long connId;

  /** エクスポートされているオブジェクト群 */
  final ExportedObjects exportedObjects;
  
  /** プロキシが作成されているリモートオブジェクト群 */
  final ProxyObjects proxyObjects;

  /** 接続IO */
  final ConnIO connIO;
  
  /** リモートから返り値もしくは例外として送られてきた値のストック */
  final ReturnStock returnStock;

  /** ローカルメソッド呼び出し中であることを記録するインターフェース */
  final LocalCalledActivity localCalledActivity;
  
  final String name;
  
  public ConnEnv(ReturnStock returnStock, ExportedObjects exportedObjects, ProxyObjects proxyObjects, 
      ConnIO connIO, long id, LocalCalledActivity calledRecorder, String name) {
    this.returnStock = returnStock;
    this.exportedObjects = exportedObjects;
    this.proxyObjects = proxyObjects;
    this.connIO = connIO;
    this.connId = id;
    this.localCalledActivity = calledRecorder;
    this.name = name;
  }  
}
