package com.cm55.lipermimod.handler;

import java.net.*;
import java.util.*;

import com.cm55.lipermimod.*;

public class CallLookup {

  

  private static Map<Thread, ConnectionImpl> connectionMap = Collections.synchronizedMap(new HashMap<Thread, ConnectionImpl>());
  
  /**
   * Get the current Socket for this call.
   * Only works in the main thread call.
   *  
   * @return Socket which started the Delegator Thread
   */
  public static Socket getCurrentSocket() {
    ConnectionImpl handler = connectionMap.get(Thread.currentThread());
    return (handler == null ? null : handler.getSocket());
  }
  
  /**
   * コネクションを取得
   * @return コネクション
   */
  public static Connection getConnection() {
     return connectionMap.get(Thread.currentThread());
  }
  
  static void handlingMe(ConnectionImpl connectionHandler) {
    synchronized (connectionMap) {
      connectionMap.put(Thread.currentThread(), connectionHandler);
    }
  }

  static void forgetMe() {
    synchronized (connectionMap) {
      connectionMap.remove(Thread.currentThread());
    }
  }
}
