package com.cm55.lipermimod.handler;

import java.io.*;

import com.cm55.lipermimod.*;

public class BytesPacket {

  static void writeBytesPacket(OutputStream out, byte[]bytes) throws IOException {
    writeSize(out, bytes.length);
    out.write(bytes);
  }

  static void writeSize(OutputStream out, int size) throws IOException {
    out.write(size >> 24);
    out.write(size >> 16);
    out.write(size >> 8);;
    out.write(size);
  }
  
  static byte[]readBytesPacket(InputStream in, String name) throws IOException {
    int size = readSize(in);
    if (size <= 0) throw new IOException(name + " Could not read size " + size);
    byte[]result = new byte[size];
    int filled = 0;
    while (filled < size) {
      int r = in.read(result, filled, size - filled);
      if (r < 0) throw new IOException("I/O Error");
      filled += r;
    }
    return result;
  }
  
  static int readSize(InputStream in) throws IOException {
    int size = 0;
    size |= readOne(in) << 24;
    size |= readOne(in) << 16;
    size |= readOne(in) << 8;
    size |= readOne(in);
    return size;
  }
  
  static int readOne(InputStream in) throws IOException {
    int read = in.read();
    if (read < 0) {
      RuntimeException ex = new LipeRMIException.Disconnected("Disconnected");
      throw ex;
    }
    return read;
  }
}
