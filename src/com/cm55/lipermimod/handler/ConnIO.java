package com.cm55.lipermimod.handler;

import java.io.*;
import java.net.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.option.*;
import com.cm55.lipermimod.util.*;
import static com.cm55.lipermimod.handler.BytesPacket.*;

/**
 * コネクションとの入出力
 * <p>
 * ある一つの{@link Socket}について、入力つまり{@link IRemoteMessage}の受信と、出力つまり{@link IRemoteMessage}の送信を行う。
 * </p>
 * @author ysugimura
 */
class ConnIO {

  static Log log = LogFactory.getLog(ConnIO.class);
  
  private InputStream input;
  private OutputStream output;
  private volatile long lastIOTime;
  private volatile SharedKey sharedKey;
  private IProtocolFilter filter;
  private int port;
  
  /** 初期化する */
  void setup(Socket socket, IProtocolFilter filter) throws IOException {
    if (log.isTraceEnabled()) log.trace("setup " + socket + "," + filter);
    this.port = socket.getPort();
    this.filter = filter;
    lastIOTime = SystemTime.get();
    
    // ※注意：なぜか入力を先にオープンするとハングしてしまう。    
    output = new OutputStreamActivity(socket.getOutputStream()) {
      @Override protected void activity(int size) {
        lastIOTime = SystemTime.get();
      }
    };
    input = new InputStreamActivity(socket.getInputStream()) {
      @Override protected void activity(int size) {
        lastIOTime = SystemTime.get();
      }
    };
  }

  public int getPort() {
    return port;
  }
  
  /** 
   * このコネクションで何らかの入出力のあった最終時刻を取得する
   * @return 最終時刻
   */
  public long getLastIOTime() {
    return lastIOTime;
  }
    
  /** 
   * リモート側にメッセージを送信する。
   * これは、向こう側のメソッド呼び出しにも使用されるし、
   * こちら側のメソッドの返り値の送信にも使用される。 
   * @param remoteMessage リモートに送信するメッセージ
   */
  void sendMessage(IRemoteMessage remoteMessage) throws IOException {
    if (log.isTraceEnabled()) log.trace("sendMessage:" + remoteMessage);
    try {
      synchronized(output) {
        // IRemoteMessageをシリアライズしてバイト配列を取得
        byte[]bytesPacket = filter.messageToBytes(remoteMessage);
        
        // バイト配列を暗号化
        if (sharedKey != null) {
          bytesPacket = sharedKey.encrypt(bytesPacket);
        }
        
        // バイト配列を送信
        writeBytesPacket(output, bytesPacket);
        output.flush();
      }
    } catch (SocketException ex) {
      throw new LipeRMIException.IOException("Could not write socket");
    } catch (IOException ex) {
      log.error("Could not write", ex);
      throw ex;
    }
  }
  
  /**
   * リモートからのメッセージを受信する
   * @return
   * @throws ClassNotFoundException
   * @throws IOException
   */
  IRemoteMessage receiveMessage(String name) throws LipeRMIException {
    
    // ソケットからバイト配列を読み込む
    byte[] bytesPacket;
    try {
      bytesPacket = readBytesPacket(input, name);
      if (log.isTraceEnabled()) log.trace("receiveMessage raw");
    } catch (SocketException ex) {
      // 既に切断されているために読み込めない。基本的には無視する
      if (log.isTraceEnabled()) log.error("Could not readUnshared from disconnected socket", ex);
      throw new LipeRMIException.Disconnected(ex.getMessage());
    } catch (IOException ex) {
      log.error("readBytesPacket ", ex);
      throw new LipeRMIException.IOException(ex.getMessage());
    }
    
    // 暗号化を解除
    if (sharedKey != null) {
      bytesPacket = sharedKey.decrypt(bytesPacket);
    }
    
    // バイト配列からIRemoteMessageオブジェクトを再構築
    IRemoteMessage remoteMessage;
    try {
      remoteMessage = filter.bytesToMessage(bytesPacket);
      if (log.isTraceEnabled()) log.trace("receiveMessage filtered " + remoteMessage);
    } catch (ClassNotFoundException ex) {
      log.error("Class not found", ex);
      throw new LipeRMIException.ClassNotFound(ex.getMessage());
    } catch (IOException ex) {
      log.error("Filter error", ex);
      throw new LipeRMIException.IOException(ex.getMessage());
    }
    
    return remoteMessage;
  }
}
