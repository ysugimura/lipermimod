package com.cm55.lipermimod.handler;

/**
 * ローカルメソッドが呼ばれている最中であることを通知するインターフェース
 * <p>
 * これは、そのメソッドの内部で自分がどのコネクションから呼ばれているかを識別するために用いられる。
 * {@link #beginMethod}が呼び出されたときのスレッドとコネクションを記録することで、メソッド内部からは、自身のスレッドからコネクションを取得できる。
 * </p>
 * @author ysugimura
 */
public interface LocalCalledActivity {
  
  /** ローカルメソッドの呼び出しを開始 */
  void beginCalled();
  
  /** ローカルメソッドの呼び出しを終了 */
  void endCalled();
}
