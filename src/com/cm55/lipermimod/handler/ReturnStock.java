package com.cm55.lipermimod.handler;

import java.util.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;

/**
 * リモート側からローカル側に送られてきた「返り値」をストックするオブジェクト。
 * <p>
 * リモート側への呼び出しの結果を常に待機するとは限らない。{@link IFuture}を使用している場合は、返り値を待たずにローカル側呼び出しが終了する。
 * このため、呼び出しIDと返り値のマップを保持する。
 * </p>
 */
class ReturnStock {

  /** call-idと{@link RemoteReturnMessage}のマップ */
  private final Map<Long, RemoteReturnMessage> returnMap = new HashMap<Long, RemoteReturnMessage>();

  /** call-idと{@link IFuture}のマップ */
  private final Map<Long, IFuture<?>> futureMap = new HashMap<>();

  /** ソケット切断フラグ */
  private volatile boolean disconnected = false;
    
  /** 
   * リモートから送られてきた返り値もしくは例外を格納する。呼び出しシーケンス番号は{@link RemoteReturnMessage}の中に記述されている。
   * これは以下のため。
   * <ul>
   * <li>{@link RemoteInvokerImpl}はリモート側にメソッド呼び出しのメッセージを送るが、その返り値もしくは例外は、{@link InputLoop}が受け取る。
   *　{@link RemoteInvokerImpl}は、本オブジェクトの{@link #getReturnMessage(long)}を使用して、その値を取得する。
   * このとき、値がやってくるまで実行はブロックされる。
   * <li>{@link IFuture}を使用した場合、{@link RemoteInvokerImpl}は値の返りを待たずに上位に制御を返すが、そのとき
   * {@link #registerFuture(long, IFuture)}を登録しておくことにより、値が取得された場合には、本オブジェクト内で{@link IFuture}をコールバックする。
   * </ul>
   */
  @SuppressWarnings("unchecked")
  public synchronized void storeReturnMessage(RemoteReturnMessage remoteReturn) {
    
    // この呼出シーケンス番号を待機中の{@link IFuture}を調べる。あればマップから削除する
    @SuppressWarnings("rawtypes")
    IFuture future = futureMap.remove(remoteReturn.callId);
    if (future != null) {
      // IFutureコールバックの呼び出し
      if (remoteReturn.throwing)
        future.exception((Throwable) remoteReturn.ret);
      else
        future.returns(remoteReturn.ret);
      return;
    }

    // 内部マップに格納しておく
    returnMap.put(remoteReturn.callId, remoteReturn);
    notifyAll();
  }

  /** 
   * 呼び出しシーケンス番号と共にIFutureを登録する。
   * 値が得られたときに、{@link IFuture}コールバックが呼び出される。
   * 
   * @param callId 呼び出しシーケンス番号
   * @param future 値が得られたときに呼び出されるコールバック
   */
  public synchronized void registerFuture(long callId, IFuture<?> future) {
    futureMap.put(callId, future);
  }

  /**
   * こちら側から向こうのメソッドを呼び出した結果の返り値を取得する。
   * たとえvoid型のメソッドでもnullではない。nullが返る場合は、ソケットクローズ時のみ。
   * 取得できない場合は、取得できるかソケットクローズまで待つ。
   * 
   * @param callId
   * @return
   */
  public synchronized RemoteReturnMessage getReturnMessage(long callId) {
    while (true) {
      RemoteReturnMessage ret = returnMap.remove(callId);
      if (ret != null)
        return ret;
      try {
        wait();
      } catch (InterruptedException ex) {
      }
      if (disconnected)
        return null;
    }
  }

  /** ソケットがクローズされたことを通知する */
  public synchronized void disconnected() {
    disconnected = true;
    notifyAll();
  }

}
