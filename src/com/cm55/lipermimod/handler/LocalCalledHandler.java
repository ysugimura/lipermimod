/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod.handler;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.exported.*;
import com.cm55.lipermimod.proxy.*;

/**
 * リモートからのローカルメソッド呼び出しを処理
 */
public class LocalCalledHandler {

  private static final Log log = LogFactory.getLog(LocalCalledHandler.class);

  /** エクスポートオブジェクトマップ */
  private ExportedObjects exportedObjects;

  public void setup(ExportedObjects exportedObjects) {
    this.exportedObjects = exportedObjects;
  }

  /**
   * リモートからのローカルオブジェクトのメソッド呼び出しを処理する。
   * 
   * @param localCall
   * @param connEnv
   * @return
   * @throws LipeRMIException
   * @throws SecurityException
   * @throws NoSuchMethodException
   * @throws IllegalArgumentException
   * @throws IllegalAccessException
   */
  public RemoteReturnMessage localCalled(final RemoteCallMessage localCall, ConnEnv connEnv) throws LipeRMIException,
      SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException {

    if (log.isTraceEnabled()) log.trace("localCalled " + localCall);

    // RemoteHandleに対応するローカルオブジェクトを取得
    IRemote localObject = exportedObjects.getFromRemoteHandle(localCall.remoteHandle, connEnv.connId);
    if (localObject == null)
      return RemoteReturnMessage.throwing(new LipeRMIException.NoImplementation("Missing RemoteHandle"),
          localCall.callId);
    if (log.isTraceEnabled()) log.trace("localObject:" + localObject.getClass());
    
    // unreferencedメソッド呼び出しの場合
    if (localCall.methodName.equals(IUnreferenced.UNREFERENCED)) {
      if (log.isTraceEnabled()) log.trace("calling unreferenced()");
      // ここではobjectのunreferencedは呼び出さない。他のコネクションからの参照の可能性あり
      exportedObjects.unreferencedFrom(localObject, connEnv.connId);
      return RemoteReturnMessage.returning(null, localCall.callId);
    }

    // メソッドを探す
    Method localMethod = findMethod(localObject, localCall.methodName);
    if (localMethod == null)
      return RemoteReturnMessage.throwing(new LipeRMIException.NoSuchMethod(localCall.methodName), localCall.callId);
    if (log.isTraceEnabled()) log.trace("calling method:" + localMethod);
    
    // メソッド呼び出しを行う
    Object returnedObject;
    try {
      returnedObject = doCalled(localObject, localMethod, localCall, connEnv.localCalledActivity);
    } catch (IllegalAccessException ex) {
      throw ex;
    } catch (Throwable cause) {
      return RemoteReturnMessage.throwing(cause, localCall.callId);
    }
    if (log.isTraceEnabled()) log.trace("returnedObject:" + returnedObject);
    
    // 返り値を変換
    return convertReturn(returnedObject, localCall.callId, connEnv);
  }

  /**
   * あるオブジェクトの指定名称のメソッドを取得する。
   * ※メソッドオーバーロードは禁止しているのでメソッド名だけでマッチングすればよい。引数を考える必要はない。
   * 
   * @param impl
   * @param methodName
   * @return
   */
  private Method findMethod(Object impl, String methodName) {
    for (Method method : impl.getClass().getMethods()) {
      if (method.getName().equals(methodName))
        return method;
    }
    throw new LipeRMIException.NoSuchMethod(methodName);
  }

  /**
   * メソッド呼び出しを行う
   * 
   * @param impl
   * @param implMethod
   * @param remoteCall
   * @return
   * @throws Throwable
   */
  private Object doCalled(Object impl, Method implMethod, RemoteCallMessage remoteCall,
      LocalCalledActivity localCalledActivity) throws Throwable {
    Class<?>[] paramTypes = implMethod.getParameterTypes();
    boolean isFuture = paramTypes.length > 0 && IFuture.class.isAssignableFrom(paramTypes[0]);

    // メソッドを呼び出し、返り値を得る
    Object returnedObject = null;
    try {
      // ローカルのメソッド呼び出しを行う
      implMethod.setAccessible(true);
      localCalledActivity.beginCalled();
      try {
        returnedObject = implMethod.invoke(impl, remoteCall.arguments);
      } finally {
        localCalledActivity.endCalled();
      }
    } catch (InvocationTargetException e) {

      // 呼び出し自体に何らかの障害があった場合
      // InvocationTargetExceptionにラップされた原因例外を取得して、送信する。
      Throwable cause = e.getCause();
      throw cause;

    } catch (IllegalArgumentException ex) {
      String args = Arrays.stream(remoteCall.arguments).map(o-> {
        if (o == null) return "null";
        return o.getClass().getName();
      }).collect(Collectors.joining(","));
      log.fatal(implMethod + "(" + args + ")", ex);
      throw ex;
    }
    return returnedObject;
  }

  /**
   * 返り値を変換する
   * 
   * @param returnedObject
   * @param callId
   * @param connection
   * @return
   */
  private RemoteReturnMessage convertReturn(Object returnedObject, long callId, ConnEnv connection) {
    assert !(returnedObject instanceof RemoteHandle);
    if (returnedObject instanceof ProxyInternal) {
      return RemoteReturnMessage.returning(((ProxyInternal) returnedObject).lipermimod_getRemoteHandle(), callId);
    } else if (returnedObject instanceof IRemote)
      return RemoteReturnMessage.returning(
          exportedObjects.ensureExportedWithRef((IRemote) returnedObject, connection.connId).objectDesc, callId);
    else
      return RemoteReturnMessage.returning(returnedObject, callId);
  }

}
