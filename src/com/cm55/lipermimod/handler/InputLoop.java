package com.cm55.lipermimod.handler;

import java.io.*;
import java.util.function.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.util.*;

/**
 * リモート側からの得られるデータ入力の処理ループ。
 * <p>
 * リモートとのコネクション{@link ConnectionImpl}を受け取り、その入力を永久ループで監視する。
 * 得られるデータは二種類であり、ローカルメソッドの呼び出し依頼と、こちらが呼び出したリモートメソッドの返り値もしくは例外である。
 * </p>
 * <p>
 * ローカルメソッド呼び出し依頼の場合には、その呼出しを行い、結果を返す。 返り値もしくは例外の場合は{@link ReturnStock}に格納する。
 * </p>
 * 
 * @author ysugimura
 */
public class InputLoop {

  private static final Log log = LogFactory.getLog(InputLoop.class);

  private ConnEnv conn;
  private LocalCalledHandler localCallHandler = new LocalCalledHandler();
  private volatile boolean isConnected = true;
  private Consumer<Throwable> disconnCallback;

  /**
   * コネクションと切断を検出した場合のコールバックを指定する
   * @param conn
   * @param disconnCallback
   */
  public void setup(ConnEnv conn, Consumer<Throwable> disconnCallback) {       
    if (log.isTraceEnabled()) log.trace("setup");
    this.conn = conn;
    this.disconnCallback = disconnCallback;
    Thread thread = new Thread(String.format("ConnectionHandler (%d)", conn.connIO.getPort())) {
      public void run() { internalRun(); }
    };
    thread.setDaemon(true);
    thread.start();
    localCallHandler.setup(conn.exportedObjects);
  }

  /** リモート側から送り込まれるメッセージを処理するループ */
  private void internalRun() {
    if (log.isTraceEnabled()) log.trace("internalRun");
    try {
      while (isConnected) {
        IRemoteMessage remoteMessage = conn.connIO.receiveMessage(conn.name);
        if (log.isTraceEnabled()) log.trace("received " + remoteMessage);
        if (remoteMessage instanceof RemoteCallMessage) {
          // ローカルメソッドの呼び出し依頼
          processLocalCall((RemoteCallMessage) remoteMessage);
        } else if (remoteMessage instanceof RemoteReturnMessage) {
          // リモートメソッド呼び出し結果の返り値あるいは例外
          conn.returnStock.storeReturnMessage((RemoteReturnMessage) remoteMessage);
        } else {
          log.error("Unknown IRemoteMessage:" + remoteMessage);
          throw new LipeRMIException.Unknown("Unknown IRemoteMessage type"); //$NON-NLS-1$
        }
      }
    } catch (Throwable th) {
      isConnected = false;
      disconnCallback.accept(th);
    }
  }

  /**
   * リモート側からのメソッド呼び出しを、ローカル側で処理し、結果をリモートに戻す。
   * 
   * @param localCall
   *          リモート側からの呼び出し
   */
  private void processLocalCall(final RemoteCallMessage localCall) {

    // 引数の中にRemoteHandleがある場合には、そのプロキシを作成してすりかえる。
    convertArgs(localCall);

    // こちら側のオブジェクトメソッドを呼び出す。
    ThreadPool.submit(new Runnable() {
      public void run() {

        RemoteReturnMessage ret;
        try {
          // 通常の呼び出し
          ret = localCallHandler.localCalled(localCall, conn);
        } catch (Exception e) {
          // 本来ここにはこないはずだが一応処理
          ret = RemoteReturnMessage.throwing(e, localCall.callId);
        }

        if (ret != null) {
          try {
            conn.connIO.sendMessage(ret);
          } catch (LipeRMIException.NotSerializable ex) {
            // 返そうとしたオブジェクトがSerializableではない。
            try {
              // 例外を戻す
              conn.connIO.sendMessage(RemoteReturnMessage.throwing(ex, ret.callId));
            } catch (IOException ex2) {}
            // サービス側でもスタックトレースを表示
            ex.printStackTrace();
          } catch (IOException ex) {
            // おそらくソケットが切断した
          }
        }
      }
    });
  }

  /**
   * リモートからのローカルメソッド呼び出しの際に、その引数が{@link ExportObjectDesc}である場合には、
   * 引数としてリモート側のオブジェクトが指定されたということ。 ローカルメソッドを呼び出す前に、この引数をプロキシに変換しておく。
   * 
   * @param localCall
   */
  private void convertArgs(RemoteCallMessage localCall) {
    Object[] args = localCall.arguments;
    for (int n = 0; n < args.length; n++) {
      Object arg = args[n];
      if (arg instanceof RemoteHandle) {
        // リモート側によって、ローカル側のオブジェクトがハンドルのみで指定された。
        arg = conn.exportedObjects.getFromRemoteHandle((RemoteHandle) arg, conn.connId);
      } else if (arg instanceof ExportObjectDesc) {
        // リモート側によって、リモート側のオブジェクトがExportObjectDescによって指定された
        arg = conn.proxyObjects.ensureProxyForObjectDesc((ExportObjectDesc) arg);
      }
      args[n] = arg;
    }

  }
}