package com.cm55.lipermimod.handler;

import java.io.*;

import org.junit.*;
import static org.junit.Assert.*;
import static com.cm55.lipermimod.handler.BytesPacket.*;

public class ConnIOTest {

  @Test
  public void test() throws IOException {
    assertEquals(0xff, fromBytes(toBytes(0xff)));
    assertEquals(0xff00, fromBytes(toBytes(0xff00)));
    assertEquals(0xff0000, fromBytes(toBytes(0xff0000)));
    assertEquals(0xff000000, fromBytes(toBytes(0xff000000)));
  }

  static byte[]toBytes(int size) throws IOException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    writeSize(out, size);
    return out.toByteArray();
  }
  
  static int fromBytes(byte[]bytes) throws IOException {
    ByteArrayInputStream in = new ByteArrayInputStream(bytes);
    return readSize(in);    
  }

}
