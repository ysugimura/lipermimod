package com.cm55.lipermimod.handler;

import java.lang.reflect.*;
import java.util.concurrent.atomic.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.proxy.*;

/**
 * リモートオブジェクトに対するメソッド呼び出しを処理する。
 * <p>
 * リモートオブジェクトに対するメソッド呼び出しは、常にローカルのプロキシから発生する。
 * このクラスは{@link com.cm55.lipermimod.proxy.RemoteInvoker}インターフェースの実装。
 * </p>
 * @author ysugimura
 */
public class RemoteInvokerImpl implements RemoteInvoker {
  
  private static final Log log = LogFactory.getLog(RemoteInvokerImpl.class);
  private static final AtomicLong callIdGenerator = new AtomicLong(0L);

  /** リモートコネクション */
  private ConnEnv conn;

  /** リモートとのコネクションを指定する */
  public RemoteInvokerImpl(ConnEnv connection) {
    this.conn = connection;
  }

  /**
   * {@link RemoteHandle}のリモートオブジェクトについて、そのメソッドを指定引数で呼び出す。
   * 実際には{@link RemoteCallMessage}を作成し、メッセージとして送信する。
   * リモート側からの返答{@link RemoteReturnMessage}を待ち、それを返す。あるいは例外を発生する。 
   * ただし、第一引数がIFutureの場合には返答を待たずにnullを返す。
   * 
   * @param remoteHandle 対象とするリモート側のオブジェクト
   * @param method　呼び出すメソッド。これはローカルにあるインターフェースのメソッドになる。
   * @param args 呼び出し引数
   * @return 
   * @throws Exception
   */
  public final Object invokeRemoteMethod(RemoteHandle handle, Method method, Object[] args) throws Exception {
    return invokeRemoteMethod(handle, method.getName(), method.getParameterTypes(), args);
  }
  
  /**
   * リモートオブジェクトについてunreferencedメソッドを呼び出す
   */
  public void invokeRemoteUnreferenced(RemoteHandle handle) throws Exception {
    invokeRemoteMethod(handle, IUnreferenced.UNREFERENCED, new Class[0], new Object[0]);
  }

  /**
   * 指定リモートオブジェクトについて、メソッド名、パラメータタイプ、引数を指定して呼び出す
   * @param handle
   * @param methodName
   * @param paramTypes
   * @param args
   * @return
   * @throws Exception
   */
  private final Object invokeRemoteMethod(RemoteHandle handle, 
      String methodName, Class<?>[]paramTypes, Object[] args) throws Exception {

    if (log.isTraceEnabled()) log.trace("invokeRemoteMethod:" + 
        handle + "," + methodName + ",");
    
    try {
      // 呼び出しIDを作成
      long callId = callIdGenerator.getAndIncrement();
      
      // 引数を変換
      if (args == null) args = new Object[0];
      IFuture<?>future = convertArgs(args, paramTypes);

      // メソッド呼び出しメッセージを送信
      conn.connIO.sendMessage(new RemoteCallMessage(handle, methodName, args, callId));

      // 第一引数がIFutureの場合は返答を待たず、返り値をnullとする
      if (future != null) {
        conn.returnStock.registerFuture(callId, future);
        return null;
      }

      // 返り値の処理。ここではリモート側から返り値が来るまで待つ。
      return waitReturn(callId);

    } finally {
      if (log.isTraceEnabled()) log.trace("Exit remoteInvocation " + methodName);
    }
  }
  
  /**
   * ローカルからの呼び出しで与えられたメソッド引数を、リモート呼び出し用に変換する。
   * 引数の中にリモートオブジェクトがあれば{@link RemoteHandle}にすりかえる。
   * 第一引数が{@link IFuture}であれば、それを返し、呼び出し引数としてはnullを設定する。
   * @param args
   * @param method
   * @return
   */
  private IFuture<?> convertArgs(Object[]args, Class<?>[] paramTypes) {
    IFuture<?> future = null;
    for (int n = 0; n < args.length; n++) {
      Object arg = args[n];
      if (arg instanceof ProxyInternal) {
        // 既にプロキシとなっているリモート側オブジェクトを、リモード側に送る場合、ハンドルのみにする
        arg = ((ProxyInternal)arg).lipermimod_getRemoteHandle();
      } else if (arg instanceof IRemote) {
        // ローカル側のIRemoteオブジェクトをリモート側に送る場合、ExportObjectDescを送る
        arg = conn.exportedObjects.ensureExportedWithRef((IRemote) arg, conn.connId).objectDesc;
      } else if (arg instanceof IFuture) {
        future = (IFuture<?>) arg;
        arg = null;
      }
      args[n] = arg;
    }
    return future;
  }
  
  /**
   * リモート側からの返り値を待ち、それを処理する。
   * @param callId 呼び出しシーケンス番号
   * @return ローカルの呼び出し側に返すオブジェクト
   * @throws Exception
   */
  private Object waitReturn(long callId) throws Exception {
    
    // リモート側からの返り値を待つ。
    RemoteReturnMessage remoteReturn = conn.returnStock.getReturnMessage(callId);

    // コネクションがドロップ
    if (remoteReturn == null) {
      if (log.isTraceEnabled()) log.trace("disconnected");
      throw new LipeRMIException.IOException("Connection aborted");
    }

    // 例外が返された場合
    if (remoteReturn.throwing && remoteReturn.ret instanceof Throwable) {
      Throwable th = (Throwable) remoteReturn.ret;
      if (th instanceof java.lang.Error)
        throw new LipeRMIException.RemoteError(th);
      throw (Exception) th;
    }

    // リモート側から返り値が返された
    Object object = remoteReturn.ret;
    if (object instanceof RemoteHandle) {
      // リモート側によって、ローカル側オブジェクトのハンドルが返り値として示された。ローカル側オブジェクトを返す。
      return conn.exportedObjects.getFromRemoteHandle((RemoteHandle)object, conn.connId);
    }
    if (object instanceof ExportObjectDesc) {
      // リモート側によって、リモード側オブジェクトのExportObjectDescが示された。ローカル側プロキシを返す。
      return conn.proxyObjects.ensureProxyForObjectDesc((ExportObjectDesc)object);
    }
    return object;
  }
}
