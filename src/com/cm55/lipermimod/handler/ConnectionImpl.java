/*
 * LipeRMI - a light weight Internet approach for remote method invocation
 * Copyright (C) 2006  Felipe Santos Andrade
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * For more information, see http://lipermi.sourceforge.net/license.php
 * You can also contact author through lipeandrade@users.sourceforge.net
 */

package com.cm55.lipermimod.handler;

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.*;
import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.exported.*;
import com.cm55.lipermimod.option.*;
import com.cm55.lipermimod.proxy.*;

/**
 * リモート側とのコネクション実装。一つの{@link Socket}に対する入出力を行う。
 * <p>
 * 入力としては、リモート側からのローカルメソッドの実行依頼であり、こちらが呼び出したリモートメソッドの返り値もしくは例外、
 * 出力としては、その逆でリモート側メソッドの実行依頼と、依頼を受けたローカルメソッドの実行結果返り値もしくは例外となる。
 * </p>
 */
public class ConnectionImpl implements  Connection {

  private static final Log log = LogFactory.getLog(ConnectionImpl.class);
  
  /** コネクション識別番号シード */
  private static long idSeed = 1;
  
  /** コネクション識別番号。1以降の整数 */
  private long id = idSeed++;
	
	private Socket socket;
	private InputLoop inputLoop = new InputLoop();

  private ProxyObjects proxyObjects = new ProxyObjects();
  private ExportedObjects exportedObjects;
	private ReturnStock returnStock = new ReturnStock();
	private ConnIO io = new ConnIO();
	  
	/** 
	 * コネクションを作成する。
	 * クライアント側ではサーバと接続したソケットが指定され、サーバ側ではサーバソケットがacceptしたソケットが指定される。
	 * @param socket リモートとの接続ソケット
	 * @param exportedObjects
	 * @param filter
	 * @return
	 */
	public ConnectionImpl setup(Socket socket, ExportedObjects exportedObjects, IProtocolFilter filter, String name) {
	  try {
	    io.setup(socket, filter);
	  } catch (Exception ex) {
	    throw new LipeRMIException.IOException(ex);
	  }
	  this.exportedObjects = exportedObjects;
		this.socket = socket;	
		ConnEnv connEnv = new ConnEnv(
		    returnStock, exportedObjects, proxyObjects, io, id, 
		    new LocalCalledActivity() {
      @Override
      public void beginCalled() {
        CallLookup.handlingMe(ConnectionImpl.this);
      }
      @Override
      public void endCalled() {
        CallLookup.forgetMe();
      }		  
		}, name);
		
    proxyObjects.setup(new RemoteInvokerImpl(connEnv));
    inputLoop.setup(connEnv, th->  {
      // 切断時コールバック
      try {
        socket.close();
      } catch (IOException e1) {
      }
      returnStock.disconnected();
      exportedObjects.connectionClosed(ConnectionImpl.this.getId());
      for (IConnectionHandlerListener listener : listeners)
        listener.connectionClosed(th);      
    });
		return this;
	}

	/**
	 * システム・サービス用のプロキシを作成する。
	 * これはクライアント側からしか呼び出されない。
	 * @return システム・サービスオブジェクト
	 */
	public SystemService createSystemService() {
	  return this.proxyObjects.createSystemService();
	}
	
	/** 
	 * このコネクションの識別番号を返す。
	 * 
	 * コネクションが複数作成されるのは、サーバ側が複数のクライアントから接続された場合や、
	 * あるいは、クライアントが複数のサーバに同時接続する場合。
	 * @return
	 */
	public long getId() {
	  return id;
	}
	
	/** 
	 * 最終IO時刻を得る
	 * これはリモート側との最後のやりとりの時刻で、ハートビートによるリモート側の生死判断に用いる。
	 * @return　IO最終時刻
	 */
	public long getLastIOTime() {
	  return io.getLastIOTime();
	}
	
	/** ソケットを取得 */
	@Override
	public Socket getSocket() {
	  return socket;
	}

	/** 切断する */
	@Override
	public void disconnect() {
	  try {
	    socket.close();
	  } catch (IOException ex) {	  
	  }
	}
	
	/** コネクションハンドラリスナ */
  private List<IConnectionHandlerListener> listeners = 
    new LinkedList<IConnectionHandlerListener>();
   
  /** コネクションハンドラリスナの登録 */
  public void addConnectionHandlerListener(IConnectionHandlerListener listener) {
    listeners.add(listener);
  }

  /** コネクションハンドラリスナの削除 */
  public void removeConnectionHandlerListener(IConnectionHandlerListener listener) {
    listeners.remove(listener);
  }
}
