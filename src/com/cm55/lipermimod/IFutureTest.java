package com.cm55.lipermimod;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.*;

public class IFutureTest {

  Server server;
  Client client;
  List<String>seq = Collections.synchronizedList(new ArrayList<String>());
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  @Test
  public void test() throws Exception {    
    
    server.registerGlobal("serverGlobal",  new ServerGlobalImpl());
    server.bind(4455);
    
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");
    
    IFuture<String>future = new IFuture<String>() {
      public void returns(String o) {
        seq.add("returns " + o);
      }
      @Override
      public void exception(Throwable th) {        
        seq.add("exception " + th.getMessage());
      }    
    };
    seq.add("start");
    serverGlobal.hello(future, "ok");
    serverGlobal.hello(future, "ng");    
    seq.add("call finished");

    Thread.sleep(1000);
    server.close();
    
    assertArrayEquals(new String[] {
        "start",
        "call finished",
        "returns world ok",
        "exception ng"
    }, seq.toArray(new String[0]));
  }
  
  public static interface ServerGlobal extends IRemote {
    public String hello(IFuture<String> f, String text);
  }
  
  private static class ServerGlobalImpl implements ServerGlobal {
    public String hello(IFuture<String> f, String text) {
      try {
        Thread.sleep(100);
      } catch (Exception ex) {}      
      if (text.equals("ok")) return "world " +  text;
      throw new RuntimeException("ng");
    }
  }
}
