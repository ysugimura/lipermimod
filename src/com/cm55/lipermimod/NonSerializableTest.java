package com.cm55.lipermimod;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.*;

public class NonSerializableTest {


  Server server;
  Client client;
  
  @Before
  public void before() {
    server = new Server();
    client = new Client();
  }
  
  public static class NonSerializable  
  {
    
  }
  
  @Test
  public void test() throws Exception {    
    // サーバ側
    server.registerGlobal("serverGlobal",  new ServerGlobalImpl());
    server.bind(4455);

    // クライアント側
    client.connect("localhost",  4455);    
    ServerGlobal serverGlobal = client.getGlobal("serverGlobal");
    try {
      serverGlobal.get();
      fail();
    } catch (LipeRMIException.NotSerializable ex) {      
    }
    try {
      serverGlobal.put(new NonSerializable());    
      fail();
    } catch (LipeRMIException.NotSerializable ex) {
      //ex.getCause().printStackTrace();
    }
    
    server.close();
  }
  
  public static interface ServerGlobal extends IRemote {
    public void put(NonSerializable ns);
    public NonSerializable get();

  }
  
  private static class ServerGlobalImpl implements ServerGlobal {
    public void put(NonSerializable ns) {      
    }
    public NonSerializable get() {
      return new NonSerializable();
    }
  }
}
