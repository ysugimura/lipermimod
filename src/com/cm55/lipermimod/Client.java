package com.cm55.lipermimod;

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.commons.logging.*;

import com.cm55.lipermimod.call.*;
import com.cm55.lipermimod.exported.*;
import com.cm55.lipermimod.handler.*;
import com.cm55.lipermimod.option.*;

public class Client {


  private static final Log log = LogFactory.getLog(Client.class);

  /** 共有鍵暗号アルゴリズム名 */
  private String cipherName;
  
  /** 共有鍵暗号ビットビット数 */
  private int cipherBits;
  
  /** オブジェクトフィルタ */
  private IProtocolFilter filter = new DefaultFilter();
  
  /** サーバへの接続ソケット */
  private Socket socket;

  /** リスナリスト */
  private ClientListeners clientListeners = new ClientListeners();

  /** ハートビート間隔 */
  private int heartBeatInterval;
  
  private ConnectionImpl connImpl = new ConnectionImpl();
  private ExportedObjects exportedObjects = new ExportedObjects();

  private String name = "unknown " + System.identityHashCode(this);
  
  /** 暗号化モジュールを指定する */
  public synchronized Client setCipher(String name, int bits) {
    beforeConnect();
    this.cipherName = name;
    this.cipherBits = bits;
    return this;
  }

  public synchronized Client setName(String name) {
    beforeConnect();
    this.name = name;
    return this;
  }
  
  /** 
   * フィルタを指定する。{@link IProtocolFilter}を実装するオブジェクト。
   * 設定しない場合のデフォルトは{@link DefaultFilter}になる。
   * 主には通信データ量をへらすために、{@link GZipFilter}を設定する場合がある。
   * そのときには、当然サーバ側も同じフィルタを設定していなければならない。
   * この設定は接続前に行う必要がある。接続後、何らかの通信が行われた後に設定することはできない。
   * @param filter フィルタ
   */
  public synchronized Client setFilter(IProtocolFilter filter) {
    beforeConnect();
    this.filter = filter;
    return this;
  }
  
  /** 
   * ハートビート間隔（秒数）を指定する。
   * 0(デフォルト)はハートビートを送らない。
   * 当然のことながら、サーバ側でハートビートを認識するように設定する
   * 必要がある。
   */
  public synchronized Client setHeartBeatInterval(int seconds) {
    beforeConnect();
    this.heartBeatInterval = seconds;
    return this;
  }
  
  /** 接続前に指定する必要がある */
  private void beforeConnect() {
    if (socket != null) throw new IllegalStateException();
  }

  /** サーバに接続する。ホスト名あるいはIPアドレスとポート番号を指定する。 */
  public synchronized Client connect(String address, int port) throws IOException {
    if (socket != null) throw new IllegalStateException();
    socket = new Socket(address, port);
    connImpl.setup(
        socket, 
        exportedObjects,
        filter,
        name
    );
    connImpl.addConnectionHandlerListener(th->clientListeners.fireConnectionClosed(th));
      
    if (log.isTraceEnabled()) log.trace("Client " + connImpl);
    
    if (heartBeatInterval != 0) {
      new Thread() {
        public void run() {
          while (true) {
            getSystemService().heartBeat();
            try {
              Thread.sleep(heartBeatInterval * 1000L);
            } catch (InterruptedException ex) {}
          }
        }
      }.start();
    }
    
    return this;
  }

  /** IClientListenerリスナを登録 */
  public Client addClientListener(IClientListener listener) {
    clientListeners.addListener(listener);
    return this;
  }

  /** IClientListenerリスナを削除 */
  public Client removeClientListener(IClientListener listener) {   
    clientListeners.removeListener(listener);
    return this;
  } 
  
  /** ソケットをクローズ */
  public void close() throws IOException {
    socket.close();
  }
  

  /** 
   * サーバ側のシステムサービスオブジェクト
   */
  private SystemService systemService;

  /** システムサービスオブジェクトを作成する */
  private synchronized SystemService getSystemService() {
    if (systemService != null) return systemService;
    return systemService = connImpl.createSystemService();
  }

  /** 名称で指定したグローバルオブジェクトを取得する */
  @SuppressWarnings("unchecked")
  public <T> T getGlobal(String globalName) {
    if (globalName == null) throw new NullPointerException();
    return (T)getSystemService().getGlobal(globalName);
  }
  
  /** ConnectionHandlerを取得する。テスト用 */
  public ConnectionImpl getConnectionHandler() {
    return connImpl;
  }
  
  public Connection getConnection() {
    return connImpl;
  }
  
  private static class ClientListeners {
    
    private List<IClientListener> listeners = new LinkedList<IClientListener>();

    private synchronized void addListener(IClientListener listener) {
      listeners.add(listener);
    }

    private synchronized void removeListener(IClientListener listener) {
      listeners.remove(listener);
    } 
    
    private synchronized void fireConnectionClosed(Throwable th) {
      for (IClientListener listener : listeners)
        listener.disconnected();      
    }
  }
  
}
