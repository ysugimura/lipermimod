package com.cm55.lipermimod;

import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Suite.*;


@RunWith(Suite.class)
@SuiteClasses( { 
  com.cm55.lipermimod.exported.AllTest.class,

  com.cm55.lipermimod.io.AllTest.class,
  com.cm55.lipermimod.option.AllTest.class,
  ClientServerTest.class,
  IFutureTest.class,
  IUnreferencedTest.class,
  NonSerializableTest.class,
  HeartBeatTest.class,
})
public class AllTest {
  public static void main(String[] args) {
    JUnitCore.main(AllTest.class.getName());
  }
}